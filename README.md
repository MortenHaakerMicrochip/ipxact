# IPXACT, SPIRIT, FM to DITA Conversion tool

This utility can be used to convert ipxact, spirit and fm files to dita.
Just clone the repository into your local and run the appropriate scrpt (See below for reference)

**Folder reference**  

`input-xml` - This is where you will add the ipxact, spirit or fm file. This will serve as the input directory for the scripts.  
`output-dita` - This is contain the generated dita-files. Always clear the contents of this folder before running the scripts.  
`bin` - This is where the scripts are located.  
`ipxact2dita` - This is where the xslts are located.

## ipxact2dita

ipxact2dita is used to convert ipxact documents to dita.

### `Usage`
```cmd
ipxact2dita.bat <xml_filename>
```
1. Open `command prompt`
2. Change directory to bin folder from where you cloned this repository.  
   `Example`:  
   `cd c:\<path_to_ipxact_folder>\bin`
3. Run ipxact batch file.  
   `ipxact2dita.bat IPXACT_FILE`  
   *(without the .xml extension)*

After running the ipxact2dita batch script, it should generate the dita files. You can find the generated dita files in `output-dita` folder.

---
**_Note:_** Before running the script, make sure that the `output-dita` folder is empty to avoid duplicates.

## spirit2dita

spirit2dita is used to convert ipxact documents to dita.

### `Usage`
```cmd
spirit2dita.bat <xml_filename>
```
1. Open `command prompt`
2. Change directory to bin folder from where you cloned this repository.  
   `Example`:  
   `cd c:\<path_to_ipxact_folder>\bin`
3. Run spirit2dita batch file.  
   `spirit2dita.bat SPIRIT_FILE`  
   *(without the .xml extension)*

After running the spirit2dita batch script, it should generate the dita files. You can find the generated dita files in `output-dita` folder.

---
**_Note:_** Before running the script, make sure that the `output-dita` folder is empty to avoid duplicates.


## fm2dita 
fm2dita is used to convert FrameMaker DOS documents into DITA DOS documents. 
This is done via a 2 primary methods: 
    - mif2go converts the bulk of the document into DITA
    - [PDF2IPXACT](https://bitbucket.microchip.com:8443/projects/M16ARC/repos/pdf2ipxact/browse) and ipxact2dita scripts convert FrameMaker registers into SIDSC registers

See: https://confluence.microchip.com:8443/display/M16A/fm2dita+Migration+Tool for details.