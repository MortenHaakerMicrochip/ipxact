@echo off

set temppath=.
set outpath=..\dita
set dosname=%1

echo Converting graphics to SVG 
for %%a in ("%temppath%\*.eps") do (
    echo   Converting: %%a to: %outpath%\%%~na.svg
    inkscape --file "%%a" --export-plain-svg="%outpath%\%%~na.svg"
    java -jar "%~dp0\SaxonHE\saxon9he.jar" -s:"%outpath%\%%~na.svg" -xsl:"%~dp0\..\xslt\fm2dita-svg.xsl" -o:"%outpath%\%%~na.svg" -expand:off

)