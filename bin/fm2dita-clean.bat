@echo off

rem %1 = %dosname% - dos book file name (w/o extension)
call :main %1 >"..\dita\_cleanup_log.txt" 2>&1
exit \b


:main
    set temppath=.
    set outpath=..\dita
    set sdlpath=..\sdl
    set ipxpath=..\ipx2dita
    set dosname=%1
    
    echo Creating %outpath%\%dosname%_hw_interface files.  
    copy "%~dp0\template_hw_interface.dita" "%outpath%\%dosname%_hw_interface.dita"
    copy "%~dp0\template_hw_ports.dita" "%outpath%\%dosname%_hw_ports.dita"
    copy "%~dp0\template_hw_parameters.dita" "%outpath%\%dosname%_hw_parameters.dita"

    echo Deleting %outpath%\Lastpage.dita and *.css files. 
    del "%outpath%\Lastpage.dita" /Q
    del "%outpath%\*.css" /Q

    echo Deleting unused %outpath%\*.ditamap files.
    for /f "usebackq tokens=*" %%f in (`dir "%outpath%\*.ditamap" /s /b ^| findstr /v /i "%dosname%.ditamap"`) do (
        echo    "%%f"
        del "%%f"
    )

    echo Converting graphics to SVG 
    for %%a in ("%temppath%\*.eps") do (
        echo   Converting: %%a to: %outpath%\%%~na.svg
        inkscape --file "%%a" --export-plain-svg="%outpath%\%%~na.svg"
        java -jar "%~dp0\SaxonHE\saxon9he.jar" -s:"%outpath%\%%~na.svg" -xsl:"%~dp0\..\xslt\fm2dita-svg.xsl" -o:"%outpath%\%%~na.svg" -expand:off
    )

    echo Cleaning up DITA file text via fm2dita.xsl
    for %%b in ("%outpath%\*.dita*") do (
        echo   fm2ditaCleanup: "%%b" 
        java -jar "%~dp0\SaxonHE\saxon9he.jar" -s:"%%b" -xsl:"%~dp0\..\xslt\fm2dita.xsl" -o:"%%b" -expand:off
    )

    SET dosfile="%ipxpath%\%dosname%.xml"
    SET ipxfile="%ipxpath%\%dosname%_ipxact.xml"
    SET logfile="%ipxpath%\%dosname%_ipxact.log"
    SET sidscfile="%outpath%\%dosname%-sidsc.ditamap"

    echo Creating SIDSC files 
    echo    DOS input: %dosfile%
    echo    IPX output: %ipxfile%
    echo    PDF2IPXACT Logged messages: %logfile%
    echo    DITA output: %sidscfile%

    java -jar "%~dp0..\pdf2ipxact\bin\PDF2IPXACT.jar"  -reg -dos %dosfile% -output %ipxfile% -log %logfile%
    java -jar "%~dp0\SaxonHE\saxon9he.jar" -s:"%ipxfile%" -xsl:"%~dp0\..\ipxact2dita\ipxact2dita.xsl" -o:"%sidscfile%" -expand:off

    mkdir %sdlpath%


