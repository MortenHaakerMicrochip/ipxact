@echo off

set temppath=.
set outpath=..\output-dita
set sdlpath=..\sdl
set ipxpath=..\input-xml
set dosname=%1
set prefix_with_module_name=%2


IF "%~3" == "true" (
	SET use_reg_desc_as_regname=true
) ELSE (
	SET use_reg_desc_as_regname=false
)

set reg_fullname_prefix=%4

set text_search=%5
set text_replacement=%6

IF "%~7" == "true" (
	SET replace_filename=true
) ELSE (
	SET replace_filename=false
)

IF "%~8" == "true" (
	SET use_bf_desc_as_bfname=true
) ELSE (
	SET use_bf_desc_as_bfname=false
)

set bfdesc_format=%9


SET dosfile="%ipxpath%\%dosname%.xml"
SET ipxfile="%ipxpath%\%dosname%_ipxact.xml"
SET logfile="%ipxpath%\%dosname%_ipxact.log"
SET sidscfile="%outpath%\%dosname%-sidsc.ditamap"

echo Copying %dosfile% to %ipxfile%
copy %dosfile% %ipxfile%


echo Creating SIDSC files 
echo    DOS input: %dosfile%
echo    IPX output: %ipxfile%
echo    PDF2IPXACT Logged messages: %logfile%
echo    DITA output: %sidscfile%

echo -jar "%~dp0..\pdf2ipxact\bin\PDF2IPXACT.jar"  -reg -dos %dosfile% -output %ipxfile% -log %logfile%
java -jar "%~dp0..\pdf2ipxact\bin\PDF2IPXACT.jar"  -reg -dos %dosfile% -output %ipxfile% -log %logfile%
echo -jar "%~dp0\SaxonHE\saxon9he.jar" -s:"%ipxfile%" -xsl:"%~dp0\..\ipxact2dita\ipxact2dita.xsl" -o:"%sidscfile%" -expand:off
java -jar "%~dp0\SaxonHE\saxon9he.jar" -s:"%ipxfile%" -xsl:"%~dp0\..\ipxact2dita\ipxact2dita.xsl" -o:"%sidscfile%" -expand:off prefix_with_module_name=%prefix_with_module_name% use_reg_desc_as_regname=%use_reg_desc_as_regname% reg_fullname_prefix=%reg_fullname_prefix% convert_note_to_element=%convert_note_to_element% text_search=%text_search% text_replacement=%text_replacement% replace_filename=%replace_filename% use_bf_desc_as_bfname=%use_bf_desc_as_bfname% bfdesc_format=%bfdesc_format%