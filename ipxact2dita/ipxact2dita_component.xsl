<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:ipxact="http://www.accellera.org/XMLSchema/IPXACT/1685-2014"
    xmlns:ditaarch="http://dita.oasis-open.org/architecture/2005/"
    xmlns:math="http://exslt.org/math" exclude-result-prefixes="xs ipxact math ditaarch"
    version="2.0">
    <xsl:import href="base_convert.xsl"/>
    <xsl:output method="xml" indent="yes" doctype-public="-//Atmel//DTD DITA SIDSC Component//EN"
        doctype-system=".\dita-1.2\atmel\dtd\atmel-sidsc-component.dtd"/>
    <xsl:strip-space elements="*"/>

    <!-- 'main' template matching root note, start here! -->
    <xsl:template match="/">

        <!-- run first pass to replace parameterIds->names, store output as variable for second pass -->
        <xsl:variable name="pass1Result">
            <xsl:apply-templates mode="pass1"/>
        </xsl:variable>

        <!-- take output of first pass and run it through the second pass, generating DITA -->
        <xsl:apply-templates mode="pass2" select="$pass1Result/ipxact:component"/>
    </xsl:template>


    <!-- identity transform for pass1, to create a copy of the IP-XACT XML with parameterIds replaced w/ parameter names -->
    <xsl:template match="node() | @*" mode="pass1">
        <xsl:copy>
            <xsl:copy-of copy-namespaces="no" select="@*"/>
            <xsl:apply-templates select="node() | @*" mode="pass1"/>
        </xsl:copy>
    </xsl:template>

    <!-- starting point for second pass, forming structure of output DITA based on component element -->
    <xsl:template match="ipxact:component" mode="pass2">

        <sidsc-component id="{generate-id()}">
            <componentName>
                <xsl:value-of select="ipxact:name"/>
            </componentName>
            <componentBriefDescription>
                <xsl:value-of select="ipxact:vendor"/>:<xsl:value-of select="ipxact:library"
                    />:<xsl:value-of select="ipxact:name"/>:<xsl:value-of select="ipxact:version"/>
            </componentBriefDescription>
            <componentBody>
                <componentDescription>
                    <xsl:value-of select="ipxact:description"/>
                </componentDescription>
            </componentBody>
            <xsl:apply-templates select="ipxact:memoryMaps/ipxact:memoryMap" mode="pass2"/>
        </sidsc-component>

    </xsl:template>


    <xsl:template name="memMapTemplate" match="ipxact:memoryMap" mode="pass2">
        <memoryMap id="{generate-id()}">
            <memoryMapName>
                <xsl:value-of select="ipxact:name"/>
            </memoryMapName>
            <memoryMapBody>
                <bitsInLau>
                    <xsl:copy-of copy-namespaces="no" select="ipxact:addressUnitBits/node()"/>
                </bitsInLau>
                <memoryMapClass>
                    <xsl:value-of select="ipxact:description"/>
                </memoryMapClass>
            </memoryMapBody>
            <xsl:apply-templates select="ipxact:addressBlock" mode="pass2"/>
        </memoryMap>
    </xsl:template>


    <xsl:template name="addressBlockTemplate" match="ipxact:addressBlock" mode="pass2">
        <addressBlock id="{generate-id()}">
            <addressBlockName>
                <xsl:value-of select="ipxact:name"/>
            </addressBlockName>
            <addressBlockBriefDescription>
                <xsl:value-of select="ipxact:description"/>
            </addressBlockBriefDescription>
            <addressBlockBody>
                <addressBlockDescription/>
                <addressBlockProperties>
                    <addressBlockPropset>
                        <baseAddress>
                            <xsl:copy-of copy-namespaces="no"
                                select="ipxact:baseAddress/* | ipxact:baseAddress/text()"/>
                        </baseAddress>
                        <range>
                            <xsl:copy-of copy-namespaces="no" select="ipxact:range/node()"/>
                        </range>
                        <width>
                            <xsl:copy-of copy-namespaces="no" select="ipxact:width/node()"/>
                        </width>
                        <byteOrder>little</byteOrder>
                    </addressBlockPropset>
                </addressBlockProperties>
            </addressBlockBody>
            <xsl:apply-templates select="ipxact:register" mode="pass2"/>
        </addressBlock>
    </xsl:template>

    <xsl:template name="registerTemplate" match="ipxact:register" mode="pass2">
        <xsl:variable name="regFileName">
            <xsl:value-of select="/ipxact:component/ipxact:name"/>-<xsl:value-of
                select="ipxact:name"/>
        </xsl:variable>
        <topicref format="dita">
            <topicmeta>
                <navtitle>
                    <xsl:copy-of select="$regFileName"/>
                </navtitle>
            </topicmeta>
        </topicref>
        <xsl:result-document href="./{$regFileName}.dita">
            <register id="{generate-id()}">
                <xsl:variable name="shortDesc">
                    <xsl:value-of select="tokenize(ipxact:description, '\n')[normalize-space()][1]"
                    />
                </xsl:variable>
                <xsl:variable name="fullDesc">
                    <xsl:value-of select="ipxact:description"/>
                </xsl:variable>
                <registerName>
                    <xsl:value-of select="ipxact:name"/>
                </registerName>
                <registerNameMore>
                    <registerNameFull>
                        <xsl:value-of
                            select="tokenize(ipxact:description, '\n')[normalize-space()][1]"/>
                    </registerNameFull>
                    <registerBriefDescription/>
                </registerNameMore>
                <registerBody>
                    <registerDescription>
                        <xsl:copy-of select="replace($fullDesc, concat($shortDesc, '\n'), '')"/>
                    </registerDescription>
                    <registerProperties>
                        <registerPropset>
                            <registerBitsInLau/>
                            <addressOffset>
                                <xsl:copy-of copy-namespaces="no"
                                    select="ipxact:addressOffset/node()"/>
                            </addressOffset>
                            <registerSize>
                                <xsl:copy-of copy-namespaces="no" select="ipxact:size/node()"/>
                            </registerSize>
                            <bitOrder>descending</bitOrder>
                            <xsl:if test="ipxact:dim">
                                <dimension>
                                    <dimensionValue>
                                        <xsl:copy-of copy-namespaces="no" select="ipxact:dim/node()"
                                        />
                                    </dimensionValue>
                                    <dimensionIncrement/>
                                </dimension>
                            </xsl:if>
                        </registerPropset>
                    </registerProperties>
                </registerBody>
                <xsl:apply-templates select="ipxact:field" mode="pass2"/>
            </register>
        </xsl:result-document>
    </xsl:template>

    <xsl:template name="fieldTemplate" match="ipxact:field" mode="pass2">
        <bitField id="{generate-id()}">
            <xsl:variable name="shortDesc">
                <xsl:value-of select="tokenize(ipxact:description, '\n')[normalize-space()][1]"/>
            </xsl:variable>
            <xsl:variable name="longDesc">
                <xsl:value-of select="ipxact:description"/>
            </xsl:variable>
            <bitFieldName>
                <xsl:value-of select="ipxact:name"/>
            </bitFieldName>
            <bitFieldBriefDescription>
                <xsl:value-of select="tokenize(ipxact:description, '\n')[normalize-space()][1]"/>
            </bitFieldBriefDescription>
            <bitFieldBody>
                <bitFieldDescription>
                    <xsl:value-of select="replace($longDesc, concat($shortDesc, '\n'), '')"/>
                </bitFieldDescription>
                <bitFieldProperties>
                    <bitFieldPropset>
                        <bitWidth>
                            <xsl:copy-of copy-namespaces="no" select="ipxact:bitWidth/node()"/>
                        </bitWidth>
                        <bitOffset>
                            <xsl:copy-of copy-namespaces="no" select="ipxact:bitOffset/node()"/>
                        </bitOffset>
                        <bitFieldAccess>
                            <xsl:call-template name="bitfieldAccessTemplate"/>
                        </bitFieldAccess>
                        <bitFieldRadix>
                            <!-- read the 'b, 'h, or 'd off the reset value to determine radix -->
                            <xsl:variable name="resetRadix">
                                <xsl:value-of select="ipxact:resets/ipxact:reset/ipxact:value"/>
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="contains($resetRadix, 'b')">binary</xsl:when>
                                <xsl:when test="contains($resetRadix, 'h')">hexadecimal</xsl:when>
                                <xsl:when test="contains($resetRadix, 'd')">decimal</xsl:when>
                                <xsl:otherwise>ERROR</xsl:otherwise>
                            </xsl:choose>
                        </bitFieldRadix>
                        <xsl:apply-templates select="ipxact:resets" mode="pass2"/>
                    </bitFieldPropset>
                </bitFieldProperties>
                <xsl:if test="ipxact:enumeratedValues/ipxact:enumeratedValue">
                    <bitFieldValues>
                        <xsl:apply-templates select="ipxact:enumeratedValues/ipxact:enumeratedValue"
                            mode="pass2"/>
                    </bitFieldValues>
                </xsl:if>
            </bitFieldBody>
        </bitField>
    </xsl:template>

    <xsl:template name="enumValTemplate" match="ipxact:enumeratedValue" mode="pass2">
        <bitFieldValueGroup>
            <bitFieldValue>
                <xsl:copy-of copy-namespaces="no" select="ipxact:value/node()"/>
            </bitFieldValue>
            <bitFieldValueName>
                <xsl:value-of select="ipxact:name"/>
            </bitFieldValueName>
            <bitFieldValueDescription>
                <xsl:value-of select="ipxact:description"/>
            </bitFieldValueDescription>
        </bitFieldValueGroup>
    </xsl:template>

    <xsl:template name="bitfieldAccessTemplate">
        <xsl:variable name="access">
            <xsl:value-of select="ipxact:access"/>
        </xsl:variable>
        <xsl:variable name="mwv">
            <xsl:value-of select="ipxact:modifiedWriteValue"/>
        </xsl:variable>
        <xsl:variable name="volatile">
            <xsl:value-of select="ipxact:volatile"/>
        </xsl:variable>

        <!-- Set possible access types based on what we used in PDF2IPXACT, 
            !note that this isn't truly comprehensive of all possible IP-XACT! -->
        <xsl:choose>
            <xsl:when test="$access = 'read-write' and $mwv = 'zeroToClear'">R/C/HS</xsl:when>
            <xsl:when test="$access = 'read-write' and $mwv = 'oneToSet'">R/S/HC</xsl:when>
            <xsl:when test="$access = 'read-write' and $mwv = ''">R/W</xsl:when>
            <xsl:when test="$access = 'read-only'">R</xsl:when>
            <xsl:when test="$access = 'read-writeOnce'">P</xsl:when>
            <xsl:when test="$access = 'write-only'">W</xsl:when>
            <xsl:otherwise>???__REVIEW__</xsl:otherwise>
        </xsl:choose>

        <!-- volatile bits are either HS, HC, or HS/HC -->
        <xsl:if test="$volatile = 'true'">
            <xsl:choose>
                <xsl:when test="$access = 'read-write' and $mwv = 'zeroToClear'"/>
                <xsl:when test="$access = 'read-write' and $mwv = 'oneToSet'"/>
                <xsl:when test="$access = 'read-only'">/HS/HC</xsl:when>
                <xsl:otherwise>/HS/HC__REVIEW__</xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template name="bitFieldReset" match="ipxact:resets" mode="pass2">
        <bitFieldReset>
            <bitFieldResetValue>
                <xsl:variable name="apos">'</xsl:variable>
                <xsl:variable name="valueStr">
                    <xsl:value-of select="ipxact:reset/ipxact:value"/>
                </xsl:variable>
                <xsl:variable name="maskStr">
                    <xsl:value-of select="ipxact:reset/ipxact:mask"/>
                </xsl:variable>
                <!-- read the 'b, 'h, or 'd off the reset value to determine radix -->
                <xsl:variable name="resetRadix">
                    <xsl:choose>
                        <xsl:when test="contains($valueStr, 'b')">binary </xsl:when>
                        <xsl:when test="contains($valueStr, 'h')">hexadecimal </xsl:when>
                        <xsl:when test="contains($valueStr, 'd')">decimal </xsl:when>
                        <xsl:otherwise>ERROR </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="regex">[0-9]+'[bhd]</xsl:variable>
                <xsl:call-template name="assembleReset">
                    <xsl:with-param name="maskStr" select="replace($maskStr, $regex, '')"/>
                    <xsl:with-param name="valueStr" select="replace($valueStr, $regex, '')"/>
                    <xsl:with-param name="radix" select="$resetRadix"/>
                </xsl:call-template>
            </bitFieldResetValue>
        </bitFieldReset>
    </xsl:template>


    <!--    
        <variable varref="company-name"/> 
        <p ishcondition="GAMES=SNAKE">This is condition one</p>
        <bitField id="BITFIELD_EQS_LR1_GT" ishcondition="(ADC='NUM_ADC=2') and (ADC='HAS_MASTER-SLAVE_OPERATION=YES')">
        <ph varref="BOD12_BODCORE_REG_NAME">CORE</ph>.
        uuid_0f6bde5f_6319_4af9_8f39_31114fe91d45
        
    -->
    <!-- look for parameter UUID references and replace them with either varrefs or ishcondition -->
    <xsl:template match="//text()" mode="pass1">
        <!-- create a variable w/ context of the list of parameters for use in parameterReplaceTemplate -->
        <xsl:variable name="params" select="//ipxact:parameters"/>

        <!-- do a search for any uuid's in the current text node -->
        <xsl:analyze-string select="."
            regex="uuid_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+">
            <!-- replace UUID w/ variable reference here -->
            <xsl:matching-substring>
                <xsl:variable name="paramRef" select="."/>
                <xsl:variable name="paramName">
                    <xsl:value-of
                        select="$params/ipxact:parameter[@parameterId = $paramRef]/ipxact:name"/>
                </xsl:variable>
                <keyword keyref="{$paramName}"/>
            </xsl:matching-substring>

            <!-- otherwise just return the string text as-is -->
            <xsl:non-matching-substring>
                <xsl:value-of select="."/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>


    <!-- Function to iterate over ipxact:reset mask and value elements to combine them into a single string with 'x' where mask had 0's. -->
    <xsl:template name="assembleResetIter">
        <xsl:param name="maskStr"/>
        <xsl:param name="valueStr"/>
        <xsl:if test="$maskStr != ''">
            <xsl:variable name="maskChar" select="substring($maskStr, 1, 1)"/>
            <xsl:variable name="valueChar" select="substring($valueStr, 1, 1)"/>
            <xsl:choose>
                <xsl:when test="matches($maskChar, '0')">x</xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$valueChar"/>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:call-template name="assembleResetIter">
                <xsl:with-param name="maskStr" select="substring-after($maskStr, $maskChar)"/>
                <xsl:with-param name="valueStr" select="substring-after($valueStr, $valueChar)"/>
            </xsl:call-template>
        </xsl:if>
    </xsl:template>


    <xsl:template name="assembleReset">
        <xsl:param name="maskStr"/>
        <xsl:param name="valueStr"/>
        <xsl:param name="radix"/>

        <xsl:choose>
            <xsl:when test="matches($radix, 'binary')">
                <xsl:call-template name="assembleResetIter">
                    <xsl:with-param name="maskStr" select="$maskStr"/>
                    <xsl:with-param name="valueStr" select="$valueStr"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="matches($radix, 'hexadecimal')">
                <xsl:variable name="maskStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="16"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$maskStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="valueStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="16"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$valueStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="resetValue">
                    <xsl:call-template name="assembleResetIter">
                        <xsl:with-param name="maskStr" select="$maskStrBin"/>
                        <xsl:with-param name="valueStr" select="$valueStrBin"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:call-template name="math:base-convert">
                    <xsl:with-param name="from-base" select="2"/>
                    <xsl:with-param name="to-base" select="16"/>
                    <xsl:with-param name="value" select="$resetValue"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="matches($radix, 'decimal')">
                <xsl:variable name="maskStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="10"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$maskStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="valueStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="10"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$valueStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="resetValue">
                    <xsl:call-template name="assembleResetIter">
                        <xsl:with-param name="maskStr" select="$maskStrBin"/>
                        <xsl:with-param name="valueStr" select="$valueStrBin"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:call-template name="math:base-convert">
                    <xsl:with-param name="from-base" select="2"/>
                    <xsl:with-param name="to-base" select="10"/>
                    <xsl:with-param name="value" select="$resetValue"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <!-- ERROR -->
            </xsl:otherwise>
        </xsl:choose>


    </xsl:template>

</xsl:stylesheet>
