<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:spirit="http://www.spiritconsortium.org/XMLSchema/SPIRIT/1.1"
    xmlns:ditaarch="http://dita.oasis-open.org/architecture/2005/"
    xmlns:math="http://exslt.org/math" xmlns:kactus2="http://kactus2.cs.tut.fi"
    xmlns:mchp="http://www.microchip.com"
    exclude-result-prefixes="xs spirit math ditaarch kactus2 mchp" version="2.0">
    
    <xsl:import href="base_convert.xsl"/>
    
    <xsl:param name="prefix_with_module_name" as="xs:string" select="''"/>
    <xsl:param name="use_reg_desc_as_regname" as="xs:boolean" select="false()"/>
    
    <xsl:param name="reg_fullname_prefix" as="xs:string" select="''"/>
    
    <xsl:param name="text_search" as="xs:string" select="''"/>
    <xsl:param name="text_replacement" as="xs:string" select="''"/>
    <xsl:param name="replace_filename" as="xs:boolean" select="false()"/>
    
    
    <xsl:output method="xml" indent="yes" doctype-public="-//OASIS//DTD DITA Map//EN"
        doctype-system="map.dtd"/>
    <xsl:strip-space elements="*"/>
    
    <!-- 'main' template matching root node, start here! -->
    <xsl:template match="/">

        <!-- run first pass to replace parameterIds->names, store output as variable for second pass -->
        <xsl:variable name="preprocessText">
            <xsl:apply-templates mode="preprocessText"/>
        </xsl:variable>

        <!-- take output of first pass and run it through the second pass, generating DITA -->
        <xsl:apply-templates select="$preprocessText/spirit:component" mode="ipxact-to-dita" />
    </xsl:template>

    <!-- identity transform for replaceParamIds, to create a copy of the IP-XACT XML with parameterIds replaced w/ parameter names -->
    <xsl:template match="node()|@*" mode="preprocessText">
        <xsl:copy>
            <xsl:apply-templates select="node()|@*" mode="preprocessText"/>
        </xsl:copy>
    </xsl:template>

    <!-- look for parameter UUID references within text and replace them with either varrefs or ishcondition -->
    <xsl:template match="//text()[not(contains(.,'__NOTE__') or contains(.,'Note:'))]" mode="preprocessText">
        <!-- create a variable w/ context of the list of parameters for use in parameterReplaceTemplate -->
        <xsl:variable name="params" select="//spirit:parameters"/>
        <xsl:variable name="macroName">
            <xsl:value-of select="/spirit:component/spirit:name"/>
        </xsl:variable>
        
        <xsl:variable name="replaced">
            <xsl:choose>
                <xsl:when test="string-length($text_search) > 0">
                    <xsl:value-of select="replace(., $text_search, $text_replacement,'i')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <!-- do a search for any uuid's in the current text node -->
        <xsl:analyze-string select="$replaced"
            regex="uuid_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+">
            <!-- replace UUID w/ variable reference here -->
            <xsl:matching-substring>
                <xsl:variable name="paramRef" select="."/>
                <xsl:variable name="paramName">
                    <xsl:value-of
                        select="normalize-space($params/spirit:parameter[@parameterId = $paramRef]/spirit:name)"
                    />
                </xsl:variable>
                <xsl:variable name="paramValueTemp">
                    <xsl:value-of
                        select="normalize-space($params/spirit:parameter[@parameterId = $paramRef]/spirit:value)"
                    />
                </xsl:variable>
                <xsl:variable name="paramValue">
                    <xsl:value-of select="replace($paramValueTemp, '''', '''''')"/>
                </xsl:variable>


                <ph ishcondition="{$macroName}_{$paramName}='{$paramValue}'" outputclass="convar">
                    <xsl:value-of select="$paramName"/>
                </ph>
            </xsl:matching-substring>

            <!-- otherwise remove line breaks and replace with <p> for DITA formatting -->
            <xsl:non-matching-substring>
                <xsl:variable name="lines" select="distinct-values(tokenize($replaced, '\n'))"/>
                <xsl:for-each select="$lines">
                    <xsl:choose>
                        <xsl:when test="count($lines)>1"><p><xsl:value-of select="."/></p></xsl:when>
                        <xsl:otherwise><xsl:value-of select="."/></xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>

    <!-- look for parameter UUID references within attributes and replace with param name -->
    <xsl:template match="//@*[name() != 'parameterId']" mode="preprocessText">
        <!-- create a variable w/ context of the list of parameters for use in parameterReplaceTemplate -->
        <xsl:variable name="params" select="//spirit:parameters"/>
        <xsl:variable name="macroName">
            <xsl:value-of select="/spirit:component/spirit:name"/>
        </xsl:variable>
        <xsl:variable name="attribName" select="name(.)"/>
        <xsl:variable name="attrib" select="."/>

        <!-- do a search for any uuid's in the current attribute node -->
        <xsl:analyze-string select="."
            regex="uuid_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+_[a-f0-9]+">
            <!-- replace UUID w/ variable reference here -->
            <xsl:matching-substring>
                <xsl:variable name="paramRef" select="."/>
                <xsl:variable name="paramName">
                    <xsl:value-of
                        select="normalize-space($params/spirit:parameter[@parameterId = $paramRef]/spirit:name)"
                    />
                </xsl:variable>

                <!-- Replace attribute UUID with parameter name -->
                <xsl:attribute name="{$attribName}">
                    <xsl:value-of select="$paramName"/>
                </xsl:attribute>
            </xsl:matching-substring>

            <!-- otherwise just return the attribute as-is -->
            <xsl:non-matching-substring>
                <xsl:copy-of copy-namespaces="no" select="$attrib"/>
            </xsl:non-matching-substring>
        </xsl:analyze-string>
    </xsl:template>
    
    
    
    <!-- Any text nodes that contain __NOTE__: text should be replaced with <note> elements
         Numbered notes (e.g. __NOTE__1:) should be replaced with <note><ol> -->
    <xsl:template match="//text()[contains(.,'__NOTE__') or contains(.,'Note:')]" mode="preprocessText">
        
        <xsl:variable name="replaced">
            <xsl:choose>
                <xsl:when test="string-length($text_search) > 0">
                    <xsl:value-of select="replace(., $text_search, $text_replacement,'i')"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="."/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <!-- Split text up into tokens based on line breaks -->
        <xsl:variable name="tokens" select="distinct-values(tokenize($replaced, '\n'))"/>
        
        <!-- Find any standard text and create paragraphs -->
        <xsl:variable name="textTok">
            <xsl:for-each select="$tokens">
                <xsl:if test="not(contains(.,'__NOTE__') or contains(.,'Note:'))">
                    <p><xsl:value-of select="."/></p>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <!-- Then copy paragraphs to output text node -->
        <xsl:if test="$textTok != ''">
            <xsl:copy-of copy-namespaces="no" select="$textTok"/>
        </xsl:if>
        
        <xsl:message>
            <xsl:value-of select="$textTok"/>
        </xsl:message>
        
        <!-- Find any notes and create a numbered list from them -->
        <xsl:variable name="notesTok">
            <xsl:for-each select="$tokens">
                <xsl:if test="matches(.,'(__NOTE__[0-9]?:[\S\s]+)|(Note:\s*)')">
                    <p><xsl:value-of select="replace(.,'__NOTE__[0-9]?: |Note:\s*','')"/></p> <!-- wrap w/ paragraph tag for easier processing -->
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="notesTokDedupe" select="$notesTok/p[position()=1 or not(./text() = preceding-sibling::p/text())]" />

        <xsl:choose>
            <xsl:when test="count($notesTokDedupe)>1">
                <note><ol><xsl:for-each select="$notesTokDedupe"><li><xsl:value-of select="."/></li></xsl:for-each></ol></note>    
            </xsl:when>
            <xsl:otherwise>
                <note><xsl:value-of select="$notesTokDedupe" /></note>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>
    

    <!-- starting point for second pass, forming structure of output DITA based on component element -->
    <xsl:template match="spirit:component" mode="ipxact-to-dita">

        <xsl:variable name="map-name"><xsl:value-of select="spirit:name"/> Register Interface</xsl:variable>

        <!-- Create DITAMAP of component item topics -->
        <map id="{generate-id()}">
            <title>
                <xsl:value-of select="$map-name"/>
            </title>
            <topicmeta>
                <shortdesc>
                    <xsl:variable name="vlnv"><xsl:value-of select="spirit:vendor"/>:<xsl:value-of
                            select="spirit:library"/>:<xsl:value-of select="spirit:name"
                            />:<xsl:value-of select="spirit:version"/></xsl:variable>
                    <xsl:value-of select="$vlnv"/>
                    <xsl:value-of select="spirit:description"/>
                </shortdesc>
            </topicmeta>

            <xsl:variable name="compFileName"><xsl:value-of select="spirit:name"/>-component</xsl:variable>
            <!-- Create component file -->
            <xsl:result-document href="./{$compFileName}.dita"
                doctype-public="-//Atmel//DTD DITA SIDSC Component//EN"
                doctype-system="C:\projects\ipdm\solution\dita\dita-1.2\atmel\dtd\atmel-sidsc-component.dtd">
                <sidsc-component id="{generate-id()}">
                    <componentName>
                        <xsl:value-of select="spirit:name"/> Component
                    </componentName>
                    <componentBriefDescription>
                        <xsl:value-of select="spirit:vendor"/>:<xsl:value-of select="spirit:library"
                            />:<xsl:value-of select="spirit:name"/>:<xsl:value-of
                            select="spirit:version"/>
                    </componentBriefDescription>
                    <componentBody>
                        <componentDescription>
                            <xsl:value-of select="spirit:description"/>
                        </componentDescription>
                    </componentBody>
                    <!-- Call memory map template -->
                    <xsl:apply-templates select="spirit:memoryMaps/spirit:memoryMap" mode="ipxact-to-dita"/>
                </sidsc-component>
            </xsl:result-document>
            
            
            <!-- Create topic refs & file for port tables -->
            <xsl:apply-templates select="spirit:model/spirit:ports" mode="ipxact-to-dita"/>
            
            <!-- Create topic refs & file & KEYS! for parameter table -->
            <xsl:apply-templates select="spirit:parameters" mode="ipxact-to-dita"/>
            
            <!-- Create component map reference -->
            <topicref href="./{$compFileName}.dita" format="dita">
                <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                <topicmeta>
                    <navtitle>
                        <xsl:copy-of select="$compFileName"/>
                    </navtitle>
                </topicmeta>
                
                <!-- Call register template -->
                <xsl:apply-templates select="//spirit:register" mode="ipxact-to-dita"/>
            </topicref>
            
            <!-- Create file for Parameter variable management -->
            <xsl:call-template name="parameterVarIds" xml:space="preserve"/>

            <!-- Create file for SDL Trisoft Condition Manager -->
            <xsl:call-template name="parameter-to-condition"/>
        </map>
    </xsl:template>


    <xsl:template name="memMapTemplate" match="spirit:memoryMap" mode="ipxact-to-dita">


        <!-- Create memory map file -->
        <!--
            <xsl:variable name="memmapFileName"><xsl:value-of select="/spirit:component/spirit:name"
                />-<xsl:value-of select="spirit:name"/>-memoryMap</xsl:variable>

            <xsl:result-document href="./{$memmapFileName}.dita"
            doctype-public="-//Atmel//DTD DITA SIDSC Memory Map//EN"
            doctype-system="C:\projects\ipdm\solution\dita\dita-1.2\atmel\dtd\atmel-sidsc-memoryMap.dtd">
        -->
            <memoryMap id="{generate-id()}">
                <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                <memoryMapName>
                    <xsl:value-of select="spirit:name"/>
                </memoryMapName>
                <memoryMapBody>
                    <bitsInLau>
                        <xsl:copy-of copy-namespaces="no" select="spirit:addressUnitBits/node()"/>
                    </bitsInLau>
                    <memoryMapClass>
                        <xsl:value-of select="spirit:description"/>
                    </memoryMapClass>
                </memoryMapBody>
                <!-- Call Address Block Template -->
                <xsl:apply-templates select="spirit:addressBlock" mode="ipxact-to-dita"/>
            </memoryMap>
        <!-- 
            </xsl:result-document>
         -->

        <!-- Create memory map DITA map reference -->
        <!-- 
            <topicref href="./{$memmapFileName}.dita" format="dita">
            <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
            <topicmeta>
                <navtitle>
                    <xsl:copy-of select="$memmapFileName"/>
                </navtitle>
            </topicmeta>
            
            <xsl:apply-templates select="spirit:addressBlock" mode="ipxact-to-dita"/>
        </topicref>
        -->
    </xsl:template>


    <xsl:template name="addressBlockTemplate" match="spirit:addressBlock" mode="ipxact-to-dita">

        <!-- Create address block file -->
        <!-- 
        <xsl:variable name="addrblockFileName"><xsl:value-of select="/spirit:component/spirit:name"
                />-<xsl:value-of select="spirit:name"/>-addrblock</xsl:variable>

        
        <xsl:result-document href="./{$addrblockFileName}.dita"
            doctype-public="-//Atmel//DTD DITA SIDSC Address Block//EN"
            doctype-system="C:\projects\ipdm\solution\dita\dita-1.2\atmel\dtd\atmel-sidsc-addressBlock.dtd">
        -->
            <addressBlock id="{generate-id()}">
                <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                <addressBlockName>
                    <xsl:value-of select="spirit:name"/>
                </addressBlockName>
                <addressBlockBriefDescription>
                    <xsl:value-of select="spirit:description"/>
                </addressBlockBriefDescription>
                <addressBlockBody>
                    <addressBlockDescription/>
                    <addressBlockProperties>
                        <addressBlockPropset>
                            <baseAddress>
                                <xsl:copy-of copy-namespaces="no"
                                    select="spirit:baseAddress/* | spirit:baseAddress/text()"/>
                            </baseAddress>
                            <range>
                                <xsl:copy-of copy-namespaces="no" select="spirit:range/node()"/>
                            </range>
                            <width>
                                <xsl:copy-of copy-namespaces="no" select="spirit:width/node()"/>
                            </width>
                            <byteOrder>little</byteOrder>
                        </addressBlockPropset>
                    </addressBlockProperties>
                </addressBlockBody>
            </addressBlock>
        
            
        <!-- 
        </xsl:result-document>
        -->

        <!-- Create address block map reference -->
        <!-- 
            <topicref href="./{$addrblockFileName}.dita" format="dita">
            <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
            <topicmeta>
                <navtitle>
                    <xsl:copy-of select="$addrblockFileName"/>
                </navtitle>
            </topicmeta>
            <xsl:apply-templates select="spirit:register" mode="ipxact-to-dita"/>
        </topicref>
        -->
    </xsl:template>

    <xsl:template name="register.exists">
        <xsl:param name="regname"/>
        <xsl:param name="ext" select="0"/>
        
        <xsl:variable name="name" select="if ($ext > 0) then concat($regname,'_DUPLICATE_',$ext) else $regname"/>
        <xsl:choose>
            <xsl:when test="not(unparsed-text-available(resolve-uri(concat('./../output-dita/',$name,'.dita'))))"><xsl:value-of select="$name"/></xsl:when>
            <xsl:otherwise>
                <xsl:call-template name="register.exists">
                    <xsl:with-param name="regname" select="$regname"/>
                    <xsl:with-param name="ext" select="$ext + 1"/>
                </xsl:call-template>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:template name="registerTemplate" match="//spirit:register" mode="ipxact-to-dita">

        <xsl:variable name="regFileName">
            <xsl:value-of select="/spirit:component/spirit:name"/>-<xsl:value-of
                select="spirit:name"/>
        </xsl:variable>
        
        <xsl:variable name="regFinalName">
            <xsl:call-template name="register.exists">
                <xsl:with-param name="regname" select="$regFileName"/>
            </xsl:call-template>
        </xsl:variable>
        
                <!-- Create register files -->
        <xsl:result-document href="./{normalize-space($regFinalName)}.dita"
            doctype-public="-//Atmel//DTD DITA SIDSC Component//EN"
            doctype-system="C:\projects\ipdm\solution\dita\dita-1.2\atmel\dtd\atmel-sidsc-component.dtd" indent="no">
            
            <xsl:variable name="apos">'</xsl:variable>
            
            <register id="{generate-id()}">
                <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                <xsl:variable name="shortDesc">
                    <xsl:value-of select="spirit:description/p[1][normalize-space()]" />
                </xsl:variable>
                <xsl:variable name="fullDesc" select="spirit:description/node()" />
                <registerName>
                    <xsl:choose>
                        <xsl:when test="not(empty($prefix_with_module_name))">
                            <xsl:value-of select="concat($prefix_with_module_name, spirit:name)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="spirit:name"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    
                </registerName>
                <registerNameMore>
                    <registerNameFull>
                        <xsl:choose>
                            <xsl:when test="$use_reg_desc_as_regname">
                                <xsl:choose>
                                    <xsl:when test="string-length($reg_fullname_prefix) > 0">
                                        <xsl:value-of select="concat($reg_fullname_prefix,$fullDesc[position()=1])"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="$fullDesc[position()=1]"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="string-length($reg_fullname_prefix) > 0">
                                        <xsl:value-of select="concat($reg_fullname_prefix,spirit:name)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="spirit:name"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </registerNameFull>
                    <!--<registerBriefDescription/>-->
                </registerNameMore>
                <registerBody>
                    <registerDescription>
                        <xsl:choose>
                            <xsl:when test="$use_reg_desc_as_regname">
                                <xsl:choose>
                                    <xsl:when test="contains($fullDesc[position()>1], '__TABLE__')">
                                        <xsl:call-template name="create.custom.table.from.text">
                                            <xsl:with-param name="text" select="$fullDesc[position()>1]"/>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:copy-of copy-namespaces="no" select="$fullDesc[position()>1]"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:choose>
                                    <xsl:when test="contains($fullDesc, '__TABLE__')">
                                        <xsl:call-template name="create.custom.table.from.text">
                                            <xsl:with-param name="text" select="$fullDesc"/>
                                        </xsl:call-template>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:copy-of copy-namespaces="no" select="$fullDesc"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:otherwise>
                        </xsl:choose>
                    </registerDescription>
                    <registerProperties>
                        <registerPropset>
                            <!-- BI: Removed registerBitsInLau as pankaj makes it optional -->
                            <!--<registerBitsInLau/>-->
                            <addressOffset>
                                <xsl:variable name="padLeft">
                                    <xsl:call-template name="repeat">
                                        <xsl:with-param name="output" select="'0'"/>
                                        <xsl:with-param name="count">
                                            <xsl:choose>
                                                <xsl:when test="string-length(spirit:addressOffset) &lt; 5">
                                                    <xsl:value-of select="5 - string-length(spirit:addressOffset)"/>
                                                </xsl:when>
                                                <xsl:otherwise>0</xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:with-param>
                                    </xsl:call-template>
                                </xsl:variable>
                                
                                <xsl:variable name="offset">
                                    <xsl:copy-of copy-namespaces="no"
                                        select="replace(spirit:addressOffset/node(),concat($apos,'h'),'0x')"/>
                                </xsl:variable>
                                
                                <xsl:copy-of select="(replace(upper-case($offset),'0X',concat('0x',$padLeft)))"/>
                            </addressOffset>
                            <registerSize>
                                <xsl:copy-of copy-namespaces="no" select="spirit:size/node()"/>
                            </registerSize>
                            <registerAccess>
                                <xsl:variable name="bit.fields">
                                    <xsl:for-each select="spirit:field">
                                        <xsl:apply-templates select="." mode="ipxact-to-dita"/>
                                    </xsl:for-each>
                                </xsl:variable>
                                
                                <xsl:variable name="bf.accesses" select="$bit.fields//bitFieldAccess"/>
                                <xsl:variable name="bitfield.access.equal" select="mchp:CheckIfBitfieldAccessAreEqual($bf.accesses)"/>
                                
                                <xsl:choose>
                                    <xsl:when test="$bitfield.access.equal">
                                        <xsl:variable name="first.bf" select="$bf.accesses[position()=1]/text()"/>
                                        <xsl:choose>
                                            <xsl:when test="$first.bf = ('R/C, HS', 'R/S, HC', 'R/K, HS', 'R/T', 'R/W')">Read/Write</xsl:when>
                                            <xsl:when test="$first.bf = ('R')">Read-only</xsl:when>
                                            <xsl:when test="$first.bf = ('W')">Write-only</xsl:when>
                                            <xsl:when test="$first.bf = ('P')">Read/Write Once</xsl:when>
                                            <xsl:otherwise><xsl:value-of select="$first.bf"/></xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:when>
                                    <xsl:otherwise>NEEDS_CLEANUP</xsl:otherwise>
                                </xsl:choose>
                            </registerAccess>
                            <registerResetValue>

                                <xsl:choose>
                                    <xsl:when test="spirit:resetValue">
                                        <xsl:value-of select="spirit:resetValue"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:variable name="padLeft">
                                            <xsl:call-template name="repeat">
                                                <xsl:with-param name="output" select="'0'"/>
                                                <xsl:with-param name="count">
                                                    <xsl:choose>
                                                        <xsl:when test="string-length(spirit:reset/spirit:value) &lt; 10">
                                                            <xsl:value-of select="10 - string-length(spirit:reset/spirit:value)"/>
                                                        </xsl:when>
                                                        <xsl:otherwise>0</xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:with-param>
                                            </xsl:call-template>
                                        </xsl:variable>
                                        
                                        <xsl:variable name="reset">
                                            <xsl:copy-of copy-namespaces="no"
                                                select="replace(spirit:reset/spirit:value/node(),concat($apos,'h'),'0x')"/>
                                        </xsl:variable>
                                        
                                        <xsl:copy-of select="(replace(upper-case($reset),'0X',concat('0x',$padLeft)))"/>
                                        
                                    </xsl:otherwise>
                                </xsl:choose>
                                
                                <!--<xsl:variable name="bit.fields">
<!-\-                                    <xsl:for-each select="spirit:field">-\->
                                <xsl:apply-templates select="spirit:field" mode="ipxact-to-dita"/>
                            <!-\-</xsl:for-each>-\->
                        </xsl:variable>
                        
                        <xsl:variable name="bit.fields.list" as="document-node()">
                            <xsl:document>
                                <xsl:apply-templates select="$bit.fields" mode="lineup"/>   
                            </xsl:document>
                        </xsl:variable>
                        
                        <xsl:message>
                            BITFIELDS:
                            <xsl:copy-of select="$bit.fields.list"/>
                        </xsl:message>-->
                                
                                <!-- IMPL 2 -->
                                <!--<xsl:variable name="reg.reset.value">
                                <xsl:for-each select="spirit:field">
                                    <xsl:element name="bitField">
                                        <xsl:element name="isReserved"><xsl:value-of select="contains(spirit:name, 'reserved') or contains(spirit:name, 'reserved+')"/></xsl:element>
                                        <xsl:element name="bitOffset"><xsl:value-of select="spirit:bitOffset"/></xsl:element>
                                        <xsl:element name="bitWidth"><xsl:value-of select="spirit:bitWidth"/></xsl:element>
                                        <xsl:element name="reservedValue"><xsl:value-of select="spirit:reserved"/></xsl:element>
                                        <xsl:element name="name"><xsl:value-of select="spirit:name"/></xsl:element>
                                        <xsl:element name="resetValue">
                                            <xsl:if test="spirit:resets">
                                                <xsl:variable name="rv"><xsl:apply-templates mode="get-bitfield-resets" select="spirit:resets"/></xsl:variable>
                                                <xsl:choose>
                                                    <xsl:when test="not(empty($rv)) and $rv != ''">
                                                        <xsl:value-of select="$rv"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:choose>
                                                            <xsl:when test="spirit:reserved = '0'">
                                                                <xsl:value-of select="spirit:reserved"/>
                                                            </xsl:when>
                                                            <xsl:otherwise>??? __NEEDS_REVIEW__[<xsl:value-of select="concat(spirit:name,'-',spirit:reserved)"/>]</xsl:otherwise>
                                                        </xsl:choose>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:if>
                                        </xsl:element>
                                    </xsl:element>
                                </xsl:for-each>
                            
                            <!-\-
                            <xsl:variable name="bf.resets">
                                <xsl:apply-templates mode="get-reset-value" select="$fields">
                                </xsl:apply-templates>
                            </xsl:variable>
                            -\->
                            
                        </xsl:variable>
                        
                        <xsl:variable name="reg.reset">
                        <xsl:for-each select="$reg.reset.value/bitField">
                            <xsl:sort select="number(bitOffset)" order="descending"/>
                            <xsl:variable name="rv" select="."/>
<!-\-                                    [<xsl:value-of select="$rv/name"/>: <xsl:value-of select="$rv/bitOffset"/>]-\->
                            <xsl:choose>
                                <xsl:when test="$rv/isReserved = 'true'">
                                    <xsl:call-template name="repeat">
                                        <xsl:with-param name="output" select="$rv/reservedValue"/>
                                        <xsl:with-param name="count" select="number($rv/bitWidth)"/>
                                    </xsl:call-template>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:if test="string-length($rv/resetValue) &lt; $rv/bitWidth">
                                        <xsl:call-template name="repeat">
                                            <xsl:with-param name="output" select="'0'"/>
                                            <xsl:with-param name="count" select="number($rv/bitWidth) - string-length($rv/resetValue)"/>
                                        </xsl:call-template>
                                    </xsl:if>
                                    <xsl:value-of select="$rv/resetValue"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:for-each>
                        </xsl:variable>
                        
                        <xsl:variable name="resetValue">
                            <xsl:call-template name="math:base-convert">
                                <xsl:with-param name="from-base" select="2"/>
                                <xsl:with-param name="to-base" select="16"/>
                                <xsl:with-param name="value" select="$reg.reset"/>
                            </xsl:call-template>
                        </xsl:variable>
                        
                        <xsl:choose>
                            <xsl:when test="$resetValue != ''">
                                <xsl:value-of select="concat('0x',$resetValue)"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'0x0'"/>
                            </xsl:otherwise>
                        </xsl:choose>-->
                                
                            </registerResetValue>
                            <bitOrder>descending</bitOrder>
                            <xsl:if test="spirit:dim[not(matches(text(),'0'))]">
                                <dimension>
                                    <dimensionValue>
                                        <xsl:copy-of copy-namespaces="no" select="spirit:dim/node()" />
                                    </dimensionValue>
                                    <dimensionIncrement/>
                                </dimension>
                            </xsl:if>
                        </registerPropset>
                    </registerProperties>
                </registerBody>
                <xsl:apply-templates select="spirit:field" mode="ipxact-to-dita"/>
            </register>
        </xsl:result-document>

        
        <!-- Create address block map reference -->
        <topicref href="./{normalize-space($regFileName)}.dita" format="dita">
            <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
            <topicmeta>
                <navtitle>
                    <xsl:copy-of select="$regFileName"/>
                </navtitle>
            </topicmeta>
        </topicref>

    </xsl:template>
    
    <xsl:function name="mchp:CheckIfBitfieldAccessAreEqual" as="xs:boolean">
        <xsl:param name="regList" as="node()*"/>
        <xsl:variable name="result">
            <xsl:call-template name="checkIfBitfieldAccessAreEqual">
                <xsl:with-param name="itemList" select="$regList"/>
                <xsl:with-param name="index" select="1"/>
            </xsl:call-template>
        </xsl:variable>
        
        <xsl:value-of select="$result"/>
        
    </xsl:function>
    
    <xsl:template name="checkIfBitfieldAccessAreEqual">
        <xsl:param name="itemList" as="node()*"/>
        <xsl:param name="index" as="xs:integer"/>
        <xsl:variable name="curValue" select="$itemList[position() = $index]/text()" />
        <xsl:variable name="nextVal" select="$itemList[position() = $index + 1]/text()" />
        
        <xsl:choose>
            <xsl:when test="empty($itemList)">
                <xsl:value-of select="false()"/>
            </xsl:when>
            <xsl:when test="(empty($curValue) and $index gt count($itemList)) or empty($nextVal)">
                <xsl:value-of select="true()"></xsl:value-of>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$curValue = $nextVal">
                        <xsl:call-template name="checkIfBitfieldAccessAreEqual">
                            <xsl:with-param name="itemList" select="$itemList"/>
                            <xsl:with-param name="index" select="round($index + 1)"/>
                        </xsl:call-template>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="false()"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    
    <xsl:template name="repeat">
        <xsl:param name="output" as="xs:string"/>
        <xsl:param name="count" as="xs:double"/>
        <xsl:if test="$count &gt; 0">
            <xsl:value-of select="$output" />
            <xsl:call-template name="repeat">
                <xsl:with-param name="output" select="$output" />
                <xsl:with-param name="count" select="$count - 1" />
            </xsl:call-template>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="getBitfieldResets" match="spirit:resets" mode="get-bitfield-resets">
        
        <xsl:variable name="apos">'</xsl:variable>
        <xsl:variable name="valueStr">
            <xsl:value-of select="spirit:reset/spirit:value"/>
        </xsl:variable>
        <xsl:variable name="maskStr">
            <xsl:value-of select="spirit:reset/spirit:mask"/>
        </xsl:variable>
        <!-- read the 'b, 'h, or 'd off the reset value to determine radix -->
        <xsl:variable name="resetRadix">
            <xsl:choose>
                <xsl:when test="contains($valueStr, 'b')">binary </xsl:when>
                <xsl:when test="contains($valueStr, 'h')">hexadecimal </xsl:when>
                <xsl:when test="contains($valueStr, 'd')">decimal </xsl:when>
                <xsl:otherwise>ERROR </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        
        <xsl:variable name="regex">[0-9]+'[bhd]</xsl:variable>
        <xsl:variable name="regexClean">[0-9]*'[bhd]</xsl:variable>
        
        <xsl:variable name="size-and-radix">
            <xsl:analyze-string select="$valueStr" regex="{$regex}">
                <xsl:matching-substring>
                    <xsl:value-of select="."/>
                </xsl:matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <!-- output the size and radix info in front of the reset details -->
        <xsl:value-of select="$size-and-radix" />
        <xsl:call-template name="assembleReset">
            <xsl:with-param name="maskStr" select="replace($maskStr, $regexClean, '')"/>
            <xsl:with-param name="valueStr" select="replace($valueStr, $regexClean, '')"/>
            <xsl:with-param name="radix" select="$resetRadix"/>
        </xsl:call-template>
        
    </xsl:template>

    <xsl:template name="fieldTemplate" match="spirit:field" mode="ipxact-to-dita">
        <xsl:choose>
            <xsl:when test="not(contains(spirit:name,'reserved_') or contains(spirit:name,'Reserved'))">
                <bitField id="{generate-id()}">
                    <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                    <xsl:variable name="firstline.has.table" select="contains(spirit:description/node()[1][normalize-space()], '__TABLE__')" />
                    <xsl:variable name="shortDesc">
                        <xsl:choose>
                            <xsl:when test="not($firstline.has.table)">
                                <xsl:value-of select="spirit:description/node()[1][normalize-space()]" />
                            </xsl:when>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:variable name="fullDesc">
                        <xsl:choose>
                            <xsl:when test="not($firstline.has.table)">
                                <xsl:value-of select="spirit:description/node()[position() > 1][normalize-space()]"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="spirit:description/node()[normalize-space()]"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <bitFieldName>
                        <xsl:value-of select="upper-case(spirit:name)"/>
                    </bitFieldName>
                    <bitFieldBriefDescription>
                        <xsl:value-of select='replace(replace($shortDesc,"\\",""), "(__TABLE__.*?__ENDTABLE__)", "")'/>
                    </bitFieldBriefDescription>
                    <bitFieldBody>
                        <xsl:choose>
                            <xsl:when test="not($fullDesc/text() = $shortDesc)">
                                <xsl:variable name="bf_desc">
                                    <xsl:copy-of copy-namespaces="no" select="$fullDesc"/>
                                </xsl:variable>
                                
                                <bitFieldDescription>
<!--                                    <xsl:copy-of select="$bf_desc"></xsl:copy-of>-->
                                    
                                    <!-- Custom code to implement table -->
                                    <xsl:call-template name="create.custom.table.from.text">
                                        <xsl:with-param name="text" select="$bf_desc"/>
                                    </xsl:call-template>
                                    
                                </bitFieldDescription>
                            </xsl:when>
                            <xsl:otherwise>
                                <bitFieldDescription/>
                            </xsl:otherwise>
                        </xsl:choose>
                        <bitFieldProperties>
                            <bitFieldPropset>
                                <bitWidth>
                                    <xsl:copy-of copy-namespaces="no" select="spirit:bitWidth/node()"/>
                                </bitWidth>
                                <bitOffset>
                                    <xsl:copy-of copy-namespaces="no" select="spirit:bitOffset/node()"/>
                                </bitOffset>
                                <bitFieldAccess>
                                    <xsl:call-template name="bitfieldAccessTemplate"/>
                                </bitFieldAccess>
                                <!-- Removing radix code as Atmel doesn't use it 3/2/17 
                        <bitFieldRadix>
                            
                            <xsl:variable name="resetRadix">
                                <xsl:value-of select="spirit:resets/spirit:reset/spirit:value"/>
                            </xsl:variable>
                            <xsl:choose>
                                <xsl:when test="contains($resetRadix, 'b')">binary</xsl:when>
                                <xsl:when test="contains($resetRadix, 'h')">hexadecimal</xsl:when>
                                <xsl:when test="contains($resetRadix, 'd')">decimal</xsl:when>
                                <xsl:otherwise>ERROR</xsl:otherwise>
                            </xsl:choose>
                        </bitFieldRadix>
                        -->
                                <xsl:apply-templates select="spirit:resets" mode="ipxact-to-dita"/>
                            </bitFieldPropset>
                        </bitFieldProperties>
                        <xsl:if test="spirit:enumeratedValues/spirit:enumeratedValue">
                            <bitFieldValues>
                                <xsl:apply-templates select="spirit:enumeratedValues/spirit:enumeratedValue"
                                    mode="ipxact-to-dita"/>
                            </bitFieldValues>
                        </xsl:if>
                    </bitFieldBody>
                </bitField>
            </xsl:when>
        </xsl:choose>  </xsl:template>

    <xsl:template name="create.custom.table.from.text">
        <xsl:param name="text" select="''"/>
        <xsl:param name="context" select="'bitfield'"/>
        
        <xsl:choose>
            <xsl:when test="contains($text, '__TABLE__')">
                
                <xsl:variable name="matches">
                    <xsl:analyze-string select="$text" regex="__TABLE__(.*?)__ENDTABLE__">
                        <xsl:matching-substring>
                            <match>
                                <xsl:value-of select="."/>
                            </match>
                        </xsl:matching-substring>
                    </xsl:analyze-string>
                </xsl:variable>
               
                        <!-- Loop over regex group --> 
                        <xsl:for-each select="$matches/match">
                            <xsl:variable name="groupContent" select="normalize-space(.)"/>
                            <table frame="all" rowsep="1" colsep="1">
                                <tgroup cols="2">
                                    <colspec colname="c1" colnum="1" colwidth="1*"/>
                                    <colspec colname="c2" colnum="2" colwidth="1*"/>
                                    <thead>
                                        <row>
                                            <entry>Value</entry>
                                            <entry>Description</entry>
                                        </row>
                                    </thead>
                                    <tbody>
                                        <xsl:analyze-string select="$groupContent" regex="__TABLE__([\s\S]*?)__ENDTABLE__">
                                            <xsl:matching-substring>
                                                <xsl:variable name="match" select="regex-group(1)"/>
                                                <xsl:variable name="row" select="tokenize($match, '¢')"/>
                                                <xsl:for-each select="$row">
                                                    <row>
                                                        <xsl:for-each select="tokenize(., '¤')">
                                                            <entry>
                                                                <xsl:value-of select="normalize-space(replace(.,'\\',''))"/>
                                                            </entry>
                                                        </xsl:for-each>
                                                    </row>
                                                </xsl:for-each>
                                            </xsl:matching-substring>
                                            <xsl:non-matching-substring>
                                            </xsl:non-matching-substring>
                                        </xsl:analyze-string>
                                    </tbody>
                                </tgroup>
                            </table>
                        </xsl:for-each>
                    
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="$context = 'bitfield'">
                        <!-- Copy the <spirit:description> element as is -->
                        <xsl:apply-templates select="$text"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="$text"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template name="enumValTemplate" match="spirit:enumeratedValue" mode="ipxact-to-dita">
        <bitFieldValueGroup>
            <bitFieldValue>
                <xsl:copy-of copy-namespaces="no" select="spirit:value/node()"/>
            </bitFieldValue>
            <xsl:if test="substring-after(spirit:name,'-') != ''">
                <bitFieldValueName>
                    <xsl:value-of select="substring-after(spirit:name,'-')"/>
                </bitFieldValueName>    
            </xsl:if>
            <bitFieldValueDescription>
                <xsl:value-of select="spirit:description"/>
            </bitFieldValueDescription>
        </bitFieldValueGroup>
    </xsl:template>

    <xsl:template name="bitfieldAccessTemplate">
        <xsl:variable name="access">
            <xsl:choose > 
                <!-- if spirit:access is not defined, default is read-write -->
                <xsl:when test="spirit:access"><xsl:value-of select="spirit:access"/></xsl:when>
                <xsl:otherwise>read-write</xsl:otherwise>
            </xsl:choose> 
        </xsl:variable>
        <xsl:variable name="mwv">
            <xsl:value-of select="spirit:modifiedWriteValue"/>
        </xsl:variable>
        <xsl:variable name="volatile">
            <xsl:value-of select="spirit:volatile"/>
        </xsl:variable>

        <!-- Set possible access types based on what we used in PDF2IPXACT, 
            !note that this isn't truly comprehensive of all possible IP-XACT! -->
        <xsl:choose>
            <xsl:when test="$access = 'read-write' and $mwv = 'zeroToClear'">R/C, HS</xsl:when>
            <xsl:when test="$access = 'read-write' and $mwv = 'oneToSet'">R/S, HC</xsl:when>
            <xsl:when test="$access = 'read-write' and $mwv = 'oneToClear'">R/K, HS</xsl:when>
            <xsl:when test="$access = 'read-write' and $mwv = 'oneToToggle'">R/T</xsl:when>
            <xsl:when test="$access = 'read-write' and $mwv = ''">R/W</xsl:when>
            <xsl:when test="$access = 'read-only'">R</xsl:when>
            <xsl:when test="$access = 'read-writeOnce'">P</xsl:when>
            <xsl:when test="$access = 'write-only'">W</xsl:when>
            <xsl:otherwise>??? __NEEDS_REVIEW__</xsl:otherwise>
        </xsl:choose>

        <!-- volatile bits are either HS, HC, or HS/HC -->
        <xsl:if test="$volatile = 'true'">
            <xsl:choose>
                <xsl:when test="$access = 'read-write' and $mwv = 'zeroToClear'"/> <!-- handled above, do nothing -->
                <xsl:when test="$access = 'read-write' and $mwv = 'oneToSet'"/> <!-- handled above, do nothing -->
                <xsl:when test="$access = 'read-write' and $mwv = 'oneToClear'"/> <!-- handled above, do nothing -->
                <xsl:when test="$access = 'read-only'">, HS/HC</xsl:when>
                <xsl:otherwise>, HS/HC __NEEDS_REVIEW__</xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:template name="bitFieldReset" match="spirit:resets" mode="ipxact-to-dita">
        <bitFieldReset>
            <bitFieldResetValue>
                <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                <xsl:variable name="apos">'</xsl:variable>
                <xsl:variable name="valueStr">
                    <xsl:value-of select="spirit:reset/spirit:value"/>
                </xsl:variable>
                <xsl:variable name="maskStr">
                    <xsl:value-of select="spirit:reset/spirit:mask"/>
                </xsl:variable>
                <!-- read the 'b, 'h, or 'd off the reset value to determine radix -->
                <xsl:variable name="resetRadix">
                    <xsl:choose>
                        <xsl:when test="contains($valueStr, 'b')">binary </xsl:when>
                        <xsl:when test="contains($valueStr, 'h')">hexadecimal </xsl:when>
                        <xsl:when test="contains($valueStr, 'd')">decimal </xsl:when>
                        <xsl:otherwise>ERROR </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                
                
                <xsl:variable name="regex">[0-9]+'[bhd]</xsl:variable>
                
                <xsl:variable name="size-and-radix">
                    <xsl:analyze-string select="$valueStr" regex="{$regex}">
                        <xsl:matching-substring>
                            <xsl:value-of select="."/>
                        </xsl:matching-substring>
                    </xsl:analyze-string>
                </xsl:variable>
                
                <!-- output the size and radix info in front of the reset details -->
                <xsl:value-of select="$size-and-radix" />
                <xsl:call-template name="assembleReset">
                    <xsl:with-param name="maskStr" select="replace($maskStr, $regex, '')"/>
                    <xsl:with-param name="valueStr" select="replace($valueStr, $regex, '')"/>
                    <xsl:with-param name="radix" select="$resetRadix"/>
                </xsl:call-template>
            </bitFieldResetValue>
        </bitFieldReset>
    </xsl:template>

    <!-- 
        assembleReset - returns a reset value with 'x' don't care values after combining spirit:value and spirit:mask elements.
                        Converts input strings to binary before combining, then converts back to input radix.
        inputs - maskStr - string representing mask value, with '<radix> removed, where 0 = don't care
                 valueStr - string represinting reset value with '<radix> removed
                 radix - string indicating radix of maskStr and valueStr, valid values are 'binary', 'hexadecimal', 'decimal'
    -->
    <xsl:template name="assembleReset">
        <xsl:param name="maskStr"/>
        <xsl:param name="valueStr"/>
        <xsl:param name="radix"/>

        <xsl:choose>
            <xsl:when test="matches($radix, 'binary')">
                <xsl:call-template name="assembleResetIter">
                    <xsl:with-param name="maskStr" select="$maskStr"/>
                    <xsl:with-param name="valueStr" select="$valueStr"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="matches($radix, 'hexadecimal')">
                <xsl:variable name="maskStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="16"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$maskStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="valueStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="16"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$valueStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="resetValue">
                    <xsl:call-template name="assembleResetIter">
                        <xsl:with-param name="maskStr" select="$maskStrBin"/>
                        <xsl:with-param name="valueStr" select="$valueStrBin"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:call-template name="math:base-convert">
                    <xsl:with-param name="from-base" select="2"/>
                    <xsl:with-param name="to-base" select="16"/>
                    <xsl:with-param name="value" select="$resetValue"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:when test="matches($radix, 'decimal')">
                <xsl:variable name="maskStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="10"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$maskStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="valueStrBin">
                    <xsl:call-template name="math:base-convert">
                        <xsl:with-param name="from-base" select="10"/>
                        <xsl:with-param name="to-base" select="2"/>
                        <xsl:with-param name="value" select="$valueStr"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="resetValue">
                    <xsl:call-template name="assembleResetIter">
                        <xsl:with-param name="maskStr" select="$maskStrBin"/>
                        <xsl:with-param name="valueStr" select="$valueStrBin"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:call-template name="math:base-convert">
                    <xsl:with-param name="from-base" select="2"/>
                    <xsl:with-param name="to-base" select="10"/>
                    <xsl:with-param name="value" select="$resetValue"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <!-- ERROR -->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    
    <!-- Function to iterate over spirit:reset mask and value elements to combine them into a single string with 'x' where mask had 0's. -->
    <xsl:template name="assembleResetIter">
        <xsl:param name="maskStr"/>
        <xsl:param name="valueStr"/>
        <xsl:choose>
            <xsl:when test="$maskStr != ''">
                <xsl:variable name="maskChar" select="substring($maskStr, 1, 1)"/>
                <xsl:variable name="valueChar" select="substring($valueStr, 1, 1)"/>
                <xsl:choose>
                    <xsl:when test="matches($maskChar, '0')">x</xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$valueChar"/>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:call-template name="assembleResetIter">
                    <xsl:with-param name="maskStr" select="substring-after($maskStr, $maskChar)"/>
                    <xsl:with-param name="valueStr" select="substring-after($valueStr, $valueChar)"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$valueStr"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    


    <!-- template for converting IPXACT port map into DITA tables -->
    <xsl:template name="portsTemplate" match="spirit:model/spirit:ports" mode="ipxact-to-dita">
        <xsl:variable name="macroName">
            <xsl:value-of select="/spirit:component/spirit:name"/>
        </xsl:variable>
        <xsl:variable name="portsFileName"><xsl:value-of select="/spirit:component/spirit:name"
            />-ports</xsl:variable>

        <topicref href="./{$portsFileName}.dita" format="dita">
            <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
            <topicmeta>
                <navtitle>
                    <xsl:copy-of select="$portsFileName"/>
                </navtitle>
            </topicmeta>
        </topicref>
        <xsl:result-document href="./{$portsFileName}.dita"
            doctype-public="-//OASIS//DTD DITA Topic//EN" doctype-system="topic.dtd">
            <topic id="{generate-id()}">
                <title><xsl:copy-of select="$macroName"/> Signal List</title>
                <body>
                    <section>Module Signals</section>
                    <simpletable>
                        <sthead>
                            <stentry>Name</stentry>
                            <stentry>Direction</stentry>
                            <stentry>Description</stentry>
                        </sthead>
                        <xsl:for-each select="spirit:port">
                            <!-- Grab parameterized port vector and array values -->
                            <xsl:variable name="portVectorLeft">
                                <xsl:copy-of
                                    select="spirit:wire/spirit:vectors/spirit:vector/spirit:left/node()"
                                />
                            </xsl:variable>
                            <xsl:variable name="portVectorRight">
                                <xsl:copy-of
                                    select="spirit:wire/spirit:vectors/spirit:vector/spirit:right/node()"
                                />
                            </xsl:variable>
                            <xsl:variable name="portArrayLeft">
                                <xsl:copy-of select="spirit:arrays/spirit:array/spirit:left/node()"
                                />
                            </xsl:variable>
                            <xsl:variable name="portArrayRight">
                                <xsl:copy-of select="spirit:arrays/spirit:array/spirit:right/node()"
                                />
                            </xsl:variable>

                            <!-- If port is wider than 1 bit or is arrayed, add vector/array values into name -->
                            <xsl:variable name="portNameText">
                                <xsl:value-of select="spirit:name"/>
                                <xsl:if test="spirit:arrays/spirit:array">
                                    <xsl:if
                                        test="not(matches(string-join($portArrayLeft//text(), ''), string-join($portArrayRight//text(), '')))"
                                            >[<xsl:value-of select="$portArrayLeft"/>:<xsl:value-of
                                            select="$portArrayRight"/>]</xsl:if>
                                </xsl:if>
                                <xsl:if
                                    test="not(matches(string-join($portVectorLeft//text(), ''), string-join($portVectorRight//text(), '')))"
                                        >[<xsl:value-of select="$portVectorLeft"/>:<xsl:value-of
                                        select="$portVectorRight"/>]</xsl:if>
                            </xsl:variable>
                            <strow>
                                <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                                <stentry>
                                    <xsl:value-of select="normalize-space($portNameText)"/>
                                </stentry>
                                <stentry>
                                    <xsl:value-of select="spirit:wire/spirit:direction"/>
                                </stentry>
                                <stentry>
                                    <xsl:value-of select="spirit:description"/>
                                </stentry>
                            </strow>
                        </xsl:for-each>
                    </simpletable>
                </body>
            </topic>
        </xsl:result-document>
    </xsl:template>

    <xsl:template name="parameterTemplate" match="spirit:parameters" mode="ipxact-to-dita">
        <xsl:variable name="macroName">
            <xsl:value-of select="/spirit:component/spirit:name"/>
        </xsl:variable>
        <xsl:variable name="paramsFileName"><xsl:value-of select="/spirit:component/spirit:name"
            />-parameters</xsl:variable>

        <topicref href="./{$paramsFileName}.dita" format="dita">
            <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
            <topicmeta>
                <navtitle>
                    <xsl:copy-of select="$paramsFileName"/>
                </navtitle>
            </topicmeta>
        </topicref>
        <xsl:result-document href="./{$paramsFileName}.dita"
            doctype-public="-//OASIS//DTD DITA Topic//EN" doctype-system="topic.dtd">
            <topic id="{generate-id()}">
                <title><xsl:copy-of select="$macroName"/> Parameter List</title>
                <body>
                    <section>Module Parameters</section>
                    <simpletable>
                        <sthead>
                            <stentry>Name</stentry>
                            <stentry>Description</stentry>
                            <stentry>Default</stentry>
                            <stentry>Valid Values</stentry>
                        </sthead>
                        <xsl:for-each select="spirit:parameter">
                            <!-- Grab parameterized parameter vector and array values -->
                            <xsl:variable name="parameterVectorLeft">
                                <xsl:copy-of
                                    select="spirit:vectors/spirit:vector/spirit:left/node()"/>
                            </xsl:variable>
                            <xsl:variable name="parameterVectorRight">
                                <xsl:copy-of
                                    select="spirit:vectors/spirit:vector/spirit:right/node()"/>
                            </xsl:variable>
                            <xsl:variable name="parameterArrayLeft">
                                <xsl:value-of
                                    select="spirit:arrays/spirit:array/spirit:left/node()/text() | @kactus2:arrayLeft"
                                />
                            </xsl:variable>
                            <xsl:variable name="parameterArrayRight">
                                <xsl:value-of
                                    select="spirit:arrays/spirit:array/spirit:right/node()/text() | @kactus2:arrayRight"
                                />
                            </xsl:variable>

                            <!-- If parameter is wider than 1 bit or is arrayed, add vector/array values into name -->
                            <xsl:variable name="parameterNameText">
                                <xsl:value-of select="spirit:name"/>
                                <xsl:if test="spirit:arrays/spirit:array | @kactus2:arrayLeft">
                                    <xsl:if
                                        test="not(matches($parameterArrayLeft, $parameterArrayRight))"
                                            >[<xsl:value-of select="$parameterArrayLeft"
                                            />:<xsl:value-of select="$parameterArrayRight"
                                        />]</xsl:if>
                                </xsl:if>
                                <xsl:if test="spirit:vectors/spirit:vector | @kactus2:arrayRight">
                                    <xsl:if
                                        test="not(matches($parameterVectorLeft, $parameterVectorRight))"
                                            >[<xsl:value-of select="$parameterVectorLeft"
                                            />:<xsl:value-of select="$parameterVectorRight"
                                        />]</xsl:if>
                                </xsl:if>
                            </xsl:variable>
                            <xsl:variable name="paramId">
                                <xsl:value-of select="@parameterId"/>
                            </xsl:variable>
                            <strow>
                                <xsl:if test="$paramId!=''">
                                    <xsl:attribute name="id"><xsl:value-of select="$paramId"/></xsl:attribute>
                                </xsl:if>
                                <xsl:apply-templates select="spirit:isPresent" mode="ipxact-to-dita"/>
                                <stentry>
                                    <xsl:value-of select="normalize-space($parameterNameText)"/>
                                </stentry>
                                <stentry>
                                    <xsl:value-of select="spirit:description"/>
                                </stentry>
                                <stentry>
                                    <xsl:value-of select="spirit:value"/>
                                </stentry>
                                <stentry/>
                            </strow>
                        </xsl:for-each>
                    </simpletable>
                </body>
            </topic>
        </xsl:result-document>
    </xsl:template>

    <xsl:template name="parameterVarIds">
        <xsl:variable name="macroName"><xsl:value-of select="/spirit:component/spirit:name"/></xsl:variable>
        <xsl:variable name="paramsFileName"><xsl:value-of select="/spirit:component/spirit:name"/>-resource</xsl:variable>
        <xsl:result-document href="./{$paramsFileName}.dita"
               doctype-public="-//OASIS//DTD DITA Topic//EN" doctype-system="topic.dtd">
            <topic id="{generate-id()}">
                <title><xsl:value-of select="$macroName"/> Parameter Variable Definitions</title>
                <shortdesc>This library includes label variables to define some Module Parameters. Each label variable is defined in a separate paragraph by means of the &lt;msgph&gt; element. The &lt;msgph&gt; attribute 'varid' should hold a unique name for the variable.</shortdesc>
                <body>   
                    <xsl:for-each select="/spirit:component/spirit:parameters/spirit:parameter" xml:space="preserve"><xsl:variable name="paramId" select="@parameterId"/>
                        <p><ph><xsl:attribute name="varid"><xsl:value-of select="replace(spirit:name, '\[[\s\S]*\]', '')"/></xsl:attribute><xsl:value-of select="spirit:value"/></ph></p>
                    </xsl:for-each>
                </body>
            </topic>
        </xsl:result-document>    
    </xsl:template>

    <!-- 
        Goal of this function is to convert typical IP-XACT isPresent expressions into valid SDL DITA conditionals
        NOTE: This is not comprehensive, it just attempts to hit the most common cases
        Examples:
        <spirit:isPresent>PARAM1 &amp; PARAM2</spirit:isPresent>
        to
        @ishcondition="PARAM1=TRUE AND PARAM2=TRUE"
        
        <spirit:isPresent>PARAM &gt;= 33</spirit:isPresent>
        to
        @ishcondition="PARAM1&gt;=33"
        
        <spirit:isPresent>PARAM==1'b0</spirit:isPresent>
        to
        @ishcondition="PARAM=FALSE"
    -->
    <xsl:template name="isPresent-to-ishcondition" match="spirit:isPresent" mode="ipxact-to-dita">
        <!-- 
            Get value of isPresent and get rid of any spaces in expression
            Convert 1'b0=>FALSE and 1'b1=>TRUE
        -->
        <xsl:variable name="apos">&apos;</xsl:variable>
        <xsl:variable name="isPresentRaw">
            <xsl:value-of
                select="replace(replace(replace(., ' ', ''), '1$aposb0', 'FALSE'), '1$aposb1', 'TRUE')"
            />
        </xsl:variable>

        <!-- Now replace & with AND and | with OR -->
        <xsl:variable name="isPresentAnd" select="replace($isPresentRaw,'[&amp;]+',' AND ')"/>
        <xsl:variable name="isPresentOr" select="replace($isPresentAnd, '[|]+', ' OR ')"/>

        <!-- Now tokenize to split apart existing expressions from AND/OR operators -->
        <xsl:variable name="isPresentToken" select="tokenize($isPresentOr, ' ')"/>

        <!-- Look through tokenized string so we can add tests to each parameter separately before we AND/OR them. -->
        <xsl:variable name="isPresentExpression">
            <xsl:for-each select="$isPresentToken">
                <xsl:choose>
                    <!-- Add AND/OR back in -->
                    <xsl:when test=". = 'AND' or . = 'OR'">
                        <xsl:value-of select="concat(' ', ., ' ')"/>
                    </xsl:when>
                    <!-- Leave testing expressions unchanged -->
                    <xsl:when test="matches(., '[&lt;&gt;=]+')">
                        <xsl:value-of select="."/>
                    </xsl:when>
                    <!-- if there is no equality test but there is a negation, make a false equality test -->
                    <xsl:when test="matches(., '!')">
                        <xsl:value-of select="."/>=FALSE </xsl:when>
                    <!-- ...if no operators, then it is a vanilla isPresent=PARAMETER true/false test case, so add =TRUE -->
                    <xsl:otherwise>
                        <xsl:value-of select="."/>=TRUE </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>

        <xsl:attribute name="ishcondition">
            <xsl:value-of select="$isPresentExpression"/>
        </xsl:attribute>

    </xsl:template>


    <xsl:template name="parameter-to-condition">
        <xsl:variable name="macroName">
            <xsl:value-of select="/spirit:component/spirit:name"/>
        </xsl:variable>
        <xsl:variable name="conditionFileName"><xsl:value-of select="/spirit:component/spirit:name"
            />-conditions</xsl:variable>
        <xsl:result-document href="./{$conditionFileName}.xml" doctype-public="" doctype-system="">
            <cm:conditions xmlns:cm="urn:trisoft.be:InfoShare:ConditionManagement:1.0"
                xmlns:mgmt="urn:trisoft.be:InfoShare:ConditionManagement:Management:1.0">


                <xsl:for-each select="//spirit:parameter">
                    <!-- Grab parameterized parameter vector and array values -->
                    <xsl:variable name="parameterVectorLeft">
                        <xsl:copy-of select="spirit:vectors/spirit:vectxor/spirit:left/node()"/>
                    </xsl:variable>
                    <xsl:variable name="parameterVectorRight">
                        <xsl:copy-of select="spirit:vectors/spirit:vector/spirit:right/node()"/>
                    </xsl:variable>
                    <xsl:variable name="parameterArrayLeft">
                        <xsl:copy-of
                            select="spirit:arrays/spirit:array/spirit:left/node() | @kactus2:arrayLeft"
                        />
                    </xsl:variable>
                    <xsl:variable name="parameterArrayRight">
                        <xsl:copy-of
                            select="spirit:arrays/spirit:array/spirit:right/node() | @kactus2:arrayRight"
                        />
                    </xsl:variable>

                    <!-- If parameter is wider than 1 bit or is arrayed, add vector/array values into name -->
                    <xsl:variable name="parameterNameText">
                        <xsl:value-of select="spirit:name"/>
                    </xsl:variable>
                    <xsl:variable name="paramId">
                        <xsl:value-of select="@parameterId"/>
                    </xsl:variable>
                    <xsl:variable name="paramValueTemp">
                        <xsl:value-of select="spirit:value"/>
                    </xsl:variable>
                    <xsl:variable name="paramValue">
                        <xsl:value-of select="replace($paramValueTemp, '''', '''''')"/>
                    </xsl:variable>

                    <xsl:variable name="paramType">
                        <xsl:call-template name="getParamType">
                            <xsl:with-param name="parameter" select="."/>
                        </xsl:call-template>
                    </xsl:variable>

                    <xsl:choose>
                        <xsl:when test="$paramType = 'NUM_RANGE'">
                            <cm:condition name="{$macroName}_{$parameterNameText}" datatype="Number"
                                range="Y" mgmt:author="IMPORTED" cm:id="{$paramId}"
                                mgmt:created="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                mgmt:modified="{format-date(current-date(),'[Y0001][M01][D01]')}">
                                <cm:label>
                                    <xsl:value-of select="$parameterNameText"/>
                                </cm:label>
                                <cm:description>
                                    <xsl:value-of select="spirit:parameter/spirit:description"/>
                                </cm:description>
                                <cm:conditionvalue cm:id="{generate-id()}"
                                    cm:name="{$parameterNameText}:{$paramValue}"
                                    mgmt:created="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    mgmt:modified="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    cm:value="{$paramValue}" mgmt:author="IMPORTED">
                                    <cm:label>
                                        <xsl:value-of select="$paramValue"/>
                                    </cm:label>
                                    <cm:description>Imported Default Value</cm:description>
                                </cm:conditionvalue>
                            </cm:condition>
                        </xsl:when>
                        <xsl:when test="$paramType = 'TEXT_BOOL'">
                            <cm:condition name="{$macroName}_{$parameterNameText}" datatype="Text"
                                range="N" mgmt:author="IMPORTED" cm:id="{$paramId}"
                                mgmt:created="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                mgmt:modified="{format-date(current-date(),'[Y0001][M01][D01]')}">
                                <cm:label>
                                    <xsl:value-of select="$parameterNameText"/>
                                </cm:label>
                                <cm:description>
                                    <xsl:value-of select="spirit:parameter/spirit:description"/>
                                </cm:description>
                                <cm:conditionvalue cm:id="{generate-id()}"
                                    cm:name="{$parameterNameText}:TRUE"
                                    mgmt:created="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    mgmt:modified="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    cm:value="TRUE" mgmt:author="IMPORTED">
                                    <cm:label>TRUE</cm:label>
                                    <cm:description>Imported - True default</cm:description>
                                </cm:conditionvalue>
                                <cm:conditionvalue cm:id="{generate-id()}"
                                    cm:name="{$parameterNameText}:FALSE"
                                    mgmt:created="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    mgmt:modified="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    cm:value="FALSE" mgmt:author="IMPORTED">
                                    <cm:label>FALSE</cm:label>
                                    <cm:description>Imported - False Default</cm:description>
                                </cm:conditionvalue>
                            </cm:condition>
                        </xsl:when>
                        <xsl:otherwise>
                            <cm:condition name="{$macroName}_{$parameterNameText}" datatype="Text"
                                range="Y" mgmt:author="IMPORTED" cm:id="{$paramId}"
                                mgmt:created="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                mgmt:modified="{format-date(current-date(),'[Y0001][M01][D01]')}">
                                <cm:label>
                                    <xsl:value-of select="$parameterNameText"/>
                                </cm:label>
                                <cm:description>
                                    <xsl:value-of select="spirit:parameter/spirit:description"/>
                                </cm:description>
                                <cm:conditionvalue cm:id="{generate-id()}"
                                    cm:name="{$parameterNameText}:{$paramValue}"
                                    mgmt:created="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    mgmt:modified="{format-date(current-date(),'[Y0001][M01][D01]')}"
                                    cm:value="{$paramValue}" mgmt:author="IMPORTED">
                                    <cm:label>
                                        <xsl:value-of select="$paramValue"/>
                                    </cm:label>
                                    <cm:description>Imported Default Value</cm:description>
                                </cm:conditionvalue>
                            </cm:condition>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>


                <cm:category cm:id="{generate-id()}"
                    xmlns:cm="urn:trisoft.be:InfoShare:ConditionManagement:1.0"
                    xmlns:mgmt="urn:trisoft.be:InfoShare:ConditionManagement:Management:1.0">
                    <cm:label>Condition categories</cm:label>
                    <cm:category cm:id="{generate-id()}">
                        <cm:label>Modules</cm:label>
                        <cm:description>Modules</cm:description>
                        <mgmt:source>SDL Group Condition Manager v.11.1.0.0</mgmt:source>
                        <cm:category cm:id="{generate-id()}">
                            <cm:label>
                                <xsl:value-of select="$macroName"/>
                            </cm:label>
                            <cm:description><xsl:value-of select="$macroName"/>
                                (Imported)</cm:description>
                            <xsl:for-each select="//spirit:parameter">

                                <xsl:variable name="parameterNameText">
                                    <xsl:value-of select="spirit:name"/>
                                </xsl:variable>
                                <xsl:variable name="paramId">
                                    <xsl:value-of select="@parameterId"/>
                                </xsl:variable>

                                <!-- If parameter is wider than 1 bit or is arrayed, add vector/array values into name -->
                                <xsl:variable name="parameterNameText">
                                    <xsl:value-of select="spirit:name"/>
                                </xsl:variable>
                                <xsl:variable name="paramValue">
                                    <xsl:value-of select="spirit:value"/>
                                </xsl:variable>
                                <cm:cref cm:c="{$macroName}_{$parameterNameText}" cm:id="{$paramId}"/>

                            </xsl:for-each>
                        </cm:category>
                    </cm:category>
                </cm:category>
            </cm:conditions>
        </xsl:result-document>
    </xsl:template>



    <xsl:template name="getParamType">
        <xsl:param name="parameter" select="spirit:parameter"/>

        <xsl:variable name="type">
            <xsl:value-of select="$parameter/@type"/>
        </xsl:variable>
        <xsl:variable name="isVector">
            <xsl:choose>
                <xsl:when test="$parameter/spirit:vectors">TRUE</xsl:when>
                <xsl:when test="$parameter/spirit:arrays">TRUE</xsl:when>
                <xsl:otherwise>FALSE</xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:variable name="pValue">
            <xsl:value-of select="spirit:value"/>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$type = 'bit' and $isVector = 'FALSE'">TEXT_BOOL</xsl:when>
            <xsl:when test="$type = 'bit' and $isVector = 'TRUE'">NUM_RANGE</xsl:when>
            <xsl:when test="$pValue = '1''b0' or $pValue = '1''b1'">TEXT_BOOL</xsl:when>
            <xsl:otherwise>NUM_RANGE</xsl:otherwise>
        </xsl:choose>

    </xsl:template>

</xsl:stylesheet>
