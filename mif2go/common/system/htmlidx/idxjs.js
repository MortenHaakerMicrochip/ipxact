function treeMenu(navid)
{
	if (typeof document.getElementById == 'undefined') { return; }

	var rollover = new Image;
	rollover.src = 'minus.gif';

	var tree = document.getElementById(navid);
	if (tree)
	{
		var items = tree.getElementsByTagName('li');
		for (var i = 0; i < items.length; i++)
		{
			treeTrigger(tree, items[i], navid);
		}

		displayReset(tree);
	}
}

var isreset = true;

function displayReset(tree)
{
	var menus = tree.getElementsByTagName('ul');
	for (var i = 0; i < menus.length; i++)
	{
		menus[i].style.display = 'none';
		menus[i].style.position = 'static';
	}
	isreset = true;
}

function treeTrigger(tree, li, navid)
{
	var a = li.getElementsByTagName('a')[0];
	var menu = li.getElementsByTagName('ul').length > 0
		? li.getElementsByTagName('ul')[0] : null;
	var menu2 = ((li.getElementsByTagName('ul').length > 1)
		&& (li.getElementsByTagName('ul')[1].parentNode == li))
		? li.getElementsByTagName('ul')[1] : null;
	var xref = null;

	if (menu)
	{
		li.className += (li.className == '' ? '' : ' ') + 'hasmenu';
	}
	else if (li.firstChild) {
		var attr = li.firstChild.getAttribute("href");
		var str = attr.substring(1,6);
		if (str == "ixnum")
		{
			xref = attr.substr(1);
		}
	}

	li.onclick = function(e)
	{
		var target = e ? e.target : window.event.srcElement;
		while (target.nodeName.toUpperCase() != 'LI')
		{
			target = target.parentNode;
		}
		if (target == this && isreset)
		{
			if (menu)
			{
				if (menu.style.display == 'none')
				{
					clearSiblingBranches(this.parentNode);
					menu.style.display = 'block';
					if (menu2)
					{
						menu2.style.display = 'block';
					}
					a.className += (a.className=='' ? '' : ' ') + 'rollover';
				}
				else
				{
					menu.style.display = 'none';
					if (menu2)
					{
						menu2.style.display = 'none';
					}
					a.className = a.className.replace(/ ?rollover/g, '');
				}
				return false;
			}
			else
			{
				if (xref)
				{
					var ref = document.getElementsByName(xref);
					if (ref.length > 0)
					{
						clearSiblingBranches(document.getElementById('indextop'));
						var par = ref[0];;
						while (par.parentNode
						 && (par.parentNode.id != 'indextop'))
						{
							par.parentNode.style.display = 'block';
							par = par.parentNode;
						}
					}
					else
					{
						return true;
					}
					return false;
				}
				else
				{
					return true;
				}
			}
		}
	};

	attachEventListener(a, 'keyup', function(e)
	{
		if (!isreset && e.keyCode == 9)
		{
			displayReset(tree);
		}
	}, false);

	var moves = 0;
	attachEventListener(a, 'mousemove', function()
	{
		if (!isreset)
		{
			moves++;
			if (moves > 2) { displayReset(tree); }
		}
	}, false);
}

function clearSiblingBranches(trigger)
{
	var menus = trigger.getElementsByTagName('ul');
	for (var i = 0; i < menus.length; i++)
	{
		menus[i].style.display = 'none';

		var a = menus[i].parentNode.getElementsByTagName('a')[0];
		if (a)
		{
			a.className = a.className.replace(/ ?rollover/g, '');
		}
	}
}


function attachEventListener(target, eventType, functionRef, capture)
{
	if (typeof target.addEventListener != 'undefined')
	{
		target.addEventListener(eventType, functionRef, capture);
	}
	else if (typeof target.attachEvent != 'undefined')
	{
		target.attachEvent('on' + eventType, functionRef);
	}
	else
	{
		eventType = 'on' + eventType;

		if (typeof target[eventType] == 'function')
		{
			var oldListener = target[eventType];

			target[eventType] = function()
			{
				oldListener();

				return functionRef();
			}
		}
		else
		{
			target[eventType] = functionRef;
		}
	}
	
	return true;
}

function addLoadListener(fn)
{
	if (typeof window.addEventListener != 'undefined')
	{
		window.addEventListener('load', fn, false);
	}
	else if (typeof document.addEventListener != 'undefined')
	{
		document.addEventListener('load', fn, false);
	}
	else if (typeof window.attachEvent != 'undefined')
	{
		window.attachEvent('onload', fn);
	}
	else
	{
		var oldfn = window.onload;
		if (typeof window.onload != 'function')
		{
			window.onload = fn;
		}
		else
		{
			window.onload = function()
			{
				oldfn();
				fn();
			};
		}
	}
}

addLoadListener(function() { treeMenu('indextop'); });
