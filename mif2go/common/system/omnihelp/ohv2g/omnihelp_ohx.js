// OmniHelp JS user-settable variables file, OHver 1.0
// *** This file is provided as a typical sample; you must
// *** edit it to include your own settings before use.

var mainName = "ohdesign.htm"

var newWindow = false
var closeWindow = false
var frameHigh = 600
var frameWide = 650
var frameOptions = ""
var frameBorder = false
var topFirst = true
var topHigh = 50
var leftWide = 230
var midHigh = 90

var showNavLeft = true
var topLeft = '<img src="ohlogo.jpg" height="25" width="50" alt="Logo" />&nbsp;OmniHelp Example'
var topButtons = true
var useStart = true
var usePrevNext = true
var useBackForward = true
var useHideShow = true
var validImg = 'ohvalidh.gif'
var validAlt = 'Valid HTML 4.01!'
var topRight = '<img src="ohvalidh.gif" border="0" alt="Valid HTML 4.01!" height="25" width="71" />'

var lowMem = true
var persistSettings = true
var ignoreCharsIX = "-.[]()<>$_"
var ignoreLeadCharsIX = ".$_<["

var ctrlCssName = "ohctrl.css"
var mainCssName = "ohdr.css"
var IECtrlCssName = "ohctrl.css"
var N6CtrlCssName = "ohctrl.css"
var N4CtrlCssName = "ohctn4.css"
var IECssName = "ohdrie.css"
var N6CssName = "ohdrn6.css"
var N4CssName = "ohdrn4.css"

var useNavToc = true
var useNavIdx = true
var useNavFts = true
var useNavRel = true

var tocGroupsOpen = false
var tocOpenLevel = 0
var tocIcoBase = "ohtc"
var tocExpand = true
var tocOpenCloseButtons = true

var idxGroupsOpen = false
var idxOpenLevel = 0
var idxIcoBase = "ohtc"
var idxExpand = true
var idxOpenCloseButtons = true
var idxSeeTerm = "see"
var idxSeeAlsoTerm = "See also"

var relShowSubjects = true

var listButton = true
var ftsHighlight = true
var ftsHighlightStyle = "background-color:yellow;"

var mergeProjects = [ 
["oh8",0,0,0,["htmlref","ag*"]],
["oh11",0,0,51,["htmllink","ab*"]]]
var mergeFirst = false

// end of omnihelp_ohx.js
