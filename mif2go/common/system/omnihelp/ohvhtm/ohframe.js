
// OmniHelp JS code referenced in head of frameset file, OHver 1.0
// Copyright (c) 2007 by Jeremy H. Griffith.  All rights reserved.

var isNav = true
var isN4 = true
var isN6 = false
var isIE = false
var isIE4 = false
var isIE5 = false

var projName = ""
var projTitle = ""
var showLeft = true
var hashName
var timeStr = ""
var topNavDoc


// functions for fast string concatenation

function Sarray(value) {
	this.sarr = new Array("")
	if (value) {
		this.add(value)
	}
}

Sarray.prototype.add = function(value) {
	if (value == 0) {
		this.sarr.push('' + value)
	} else {
		this.sarr.push(value)
	}
}

Sarray.prototype.clear = function() {
	this.sarr.length = 1
}

Sarray.prototype.out = function() {
	return this.sarr.join("")
}

// loading project settings in head

function loadVars(doc) {
	if (opener && opener.projName) {
		projName = opener.projName
		projTitle = opener.projTitle
	} else if (window.location.search) {
		parseSearch(unescape(window.location.search.substr(1)))
	}
	if (projName.length == 0) {
		getFCookies()
	}
	doc.title = projTitle
	var str = new Sarray('<script language="JavaScript" type="text/javascript" src="')
	str.add(projName)
	str.add('_ohx.js"><')
	str.add('\/script>')
	doc.writeln(str.out())
}

function parseSearch(str) {
	var args = str.split("&")
	var argval = new Array()
	for (var i = 0 ; i < args.length ; i++) {
		argval = args[i].split("=")
		if (argval.length < 2) {
			break
		}
		if (argval[0] == "projName") {
			projName = argval[1]
		}
		else if (argval[0] == "projTitle") {
			projTitle = argval[1]
		}
	}
}


// functions for loading and reloading framesets

function restartOH() {
	setFCookies()
	var str = frameDocName + "?var=" + escape(timeStr)
	window.location.replace(str)
}

function writeFramesets(doc) {
	var str = new Sarray()
	selectCSS()
	getFCookies()
	if ((showNavLeft == false) && isN4) {
		writeSimpleFrameset(doc)
	} else if (topFirst) {
		writeTopFrameset(doc)
	} else {
		writeLeftFrameset(doc)
	}

  str.add('<noframes><body>\n')
	str.add('<p class="body">')
	str.add(NoFramesMsg)
	str.add('</p>\n')
  str.add('</body></noframes></frameset></html>')
	doc.write(str.out())
	doc.close()
}

function writeSimpleFrameset(doc) {
	var str = new Sarray('<frameset id="simpleSet" rows="0, 0, ')
	str.add(topHigh)
	str.add(', *"')
	if (frameBorder == false) {
		str.add(' border="0"')
	}
	str.add(' onload="frameLoaded()">\n')
	doc.write(str.out())
	writeFrame(doc, 'ctrl', true, true, blankName)
	writeFrame(doc, 'merge', true, true, blankName)
	writeFrame(doc, 'topnav', true, false, topNavName)
	writeFrame(doc, 'main', false, false, mainDocName)
}

function writeTopFrameset(doc) {
	var str = new Sarray('<frameset id="topSet" rows="')
	str.add(topHigh)
	str.add(', *"')
	if (frameBorder == false) {
		str.add(' border="0"')
	}
	str.add(' onload="frameLoaded()">\n')
	str.add('<frameset id="topNav" rows="0, 0, *" >\n')
	doc.write(str.out())
	str.clear()
	writeFrame(doc, 'ctrl', true, true, blankName)
	writeFrame(doc, 'merge', true, true, blankName)
	writeFrame(doc, 'topnav', true, false, (isIE ? topNavName : blankName))
	str.add('</frameset>\n')
	str.add('<frameset id="leftNav" cols="')
	str.add(showNavLeft ? leftWide : '0')
	str.add(', *">\n')
 	str.add('<frameset id="leftNavCtrl" rows="')
	str.add(midHigh)
	str.add(', *">\n')
	doc.write(str.out())
	str.clear()
	writeFrame(doc, 'navctrl', true, false, navCtrlName)
	writeFrame(doc, 'nav', false, false, (isIE ? navDocName : blankName))
	str.add('</frameset>\n')
	doc.write(str.out())
	str.clear()
	writeFrame(doc, 'main', false, false, (isIE ? mainDocName : blankName))
	str.add('</frameset>\n')
	doc.write(str.out())
}

function writeLeftFrameset(doc) {
	var str = new Sarray('<frameset id="leftNav" cols="')
	str.add(showNavLeft ? leftWide : '0')
	str.add(', *"')
	if (frameBorder == false) {
		str.add(' border="0"')
	}
	str.add(' onload="frameLoaded()">\n')
	str.add('<frameset id="leftNavCtrl" rows="')
	str.add(midHigh)
	str.add(', *">\n')
	doc.write(str.out())
	str.clear()
	writeFrame(doc, 'navctrl', true, false, navCtrlName)
	writeFrame(doc, 'nav', false, false, (isIE ? navDocName : blankName))
	str.add('</frameset>\n')
	str.add('<frameset id="rightMain" rows="0, 0,')
	str.add(topHigh)
	str.add(', *" >\n')
	doc.write(str.out())
	str.clear()
	writeFrame(doc, 'ctrl', true, true, blankName)
	writeFrame(doc, 'merge', true, true, blankName)
	writeFrame(doc, 'topnav', true, false, (isIE ? topNavName : blankName))
	writeFrame(doc, 'main', false, false, (isIE ? mainDocName : blankName))
	str.add('</frameset>\n')
	doc.write(str.out())
}

function writeFrame(doc, name, noscroll, nores, src) {
	var str = new Sarray('<frame id="')
	str.add(name)
	str.add('" name="')
	str.add(name )
	str.add('" src="')
	str.add(src)
	str.add('"\n')
	str.add(' frameborder="1"'  )
	if (nores) {
		str.add(' noresize="noresize"')
	}
	str.add(' marginwidth="1" marginheight="1" ')
	if (noscroll) {
		str.add('scrolling="no" ')
	}
	str.add('/>\n')
	doc.write(str.out())
}

function getFCookies() {
	var str = unescape(document.cookie)
	var pos = str.indexOf("=")
	if (pos == -1) {
		return false
	}
	var start = 0
	var end = str.indexOf(";")
	while (end != -1) {
		getFCookVal(str.substring(start, end))
		start = end + 2
		end = str.indexOf(";", start)
	}
	getFCookVal(str.substring(start, str.length))
	return true
}

function getFCookVal(cook) {
	var pos = cook.indexOf("=")
	if (pos == -1) {
		return
	}
	var name = cook.substring(0, pos)
	var val = cook.substring(pos + 1)
	if (name == "showNavLeft") {
		showLeft = (val == "true") ? true : false
	}
	else if (name == "projName") {
		projName = val
	}
	else if (name == "projTitle") {
		projTitle = unescape(val)
	}
}

function setFCookies() {
	var str = ''
	var exp = new Date()
	var nextYear = exp.getTime() + (365 * 24 * 60 * 60 * 1000)
	exp.setTime(nextYear)
	timeStr = exp.toGMTString()
	str = '; expires=' + timeStr
	document.cookie = "showNavLeft=" + (showNavLeft ? "true" : "false") + str
}


// function used to start frameset operation

function frameLoaded() {
	var quest = 0

	if (window.location.hash) {
		hashName = window.location.hash.substr(1)
		quest = hashName.indexOf("?")
		if (quest > -1) {
			hashName = window.location.hash.substr(1, quest)
		}
	}
	ctrl.location.href = ctrlName
	topnav.location.href = topNavName
}

// functions used to set CSS per browser type

function selectCSS() {
	var ver = parseInt(navigator.appVersion)
	var v5 = false
	if (ver >= 4) {
		v5 = (parseInt(navigator.appVersion.substring(0,1)) >= 5)
		isNav = (navigator.appName == "Netscape")
		isIE = (navigator.appName.indexOf("Microsoft") != -1)
		isN4 = isNav && !v5 && !isIE
		isN6 =  v5 && !isIE
		isIE4 = isIE && (ver == 4)
		isIE5 = isIE && v5
		isNav = isNav && !isIE
	}

	if (isIE) {
		mainCssName = IECssName
		ctrlCssName = IECtrlCssName
	} else if (isN6) {
		mainCssName = N6CssName
		ctrlCssName = N6CtrlCssName
	} else if (isN4) {
		mainCssName = N4CssName
		ctrlCssName = N4CtrlCssName
	}
}

function ctrlCSS(doc) {
	var str = new Sarray('<base href="')
	str.add(parent.ctrl.location.href)
	str.add('" />\n')
	str.add('<link rel="stylesheet" href="')
	str.add(ctrlCssName)
	str.add('" type="text/css" />')
	doc.writeln(str.out())
}

function mainCSS(doc) {
	var path = parent.ctrl.location.href
	var str = new Sarray('<link rel="stylesheet" href="')
	str.add(path.substring(0, path.lastIndexOf('/') + 1))
	str.add(mainCssName)
	str.add('" type="text/css" />')
	doc.writeln(str.out())
}


// function used to produce empty files for initial load

function blank() {
	return "<html></html>"
}


// end of ohframe.js

