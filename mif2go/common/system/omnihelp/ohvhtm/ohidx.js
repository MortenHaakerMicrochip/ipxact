
// OmniHelp functions for ix construction, OHver 0.88
// Copyright (c) 2006 by Jeremy H. Griffith.  All rights reserved.

var lastIxId = ""
var lastDocIxId = ""
var lastIxShow = -1
var lastDocIxShow = -1
var idxSetGroupsOpen = false
var idxSetGroupsClosed = false
var idxScroll = ""
var firstIdxUse = true
var ixList = new Array()
var ixIdList = new Array()
var ixIDs = new Array()
var currIdxDoc = ''
var idxCtrlHigh = 62


function useIx() {
	if (parent.isN4) {
		parent.idxExpand = false
	}
	lastIxShow = 0
	currNavCtrl = 1
	writeIxCtrlFile()
	writeIxDocFile()
	setCookies()
}

// click on letter in navctrl second row
function setIxId(str) {
	currIxId = str
	writeIxCtrlFile()
	writeIxDocFile()
	setCookies()
}

// click on See ref to another part of the Idx
function setIdx(newId, hash) {
	idxScroll = escape(hash)
	if (currIxId != newId) {
		setIxId(newId)
	} else {
		idxLoaded(parent.nav.document)
	}
}

function writeIxCtrlFile() {
	if ((currIxId == lastIxId) && (lastNavCtrl == 1)) {
		return
	}

	if (parent.idxExpand && parent.idxOpenCloseButtons) {
		idxCtrlHigh = 92
	}
	var str = new Sarray()
	var obj = parent.isIE4 ? parent.document.all["leftNavCtrl"] : parent.document.getElementById("leftNavCtrl")
	str.add(idxCtrlHigh)
	str.add(', *')
	obj.rows = str.out()

	lastNavCtrl = 1
	lastIxId = currIxId
	lastIxShow = 1
	var doc = parent.navctrl.document
	doc.open()
	doc.write(parent.docHeader)
	doc.write('<head><title>OmniHelp Nav Control: Index, OHver 0.8</title>\n')
	parent.ctrlCSS(doc)
	doc.write('</head>\n<body class="navctrl"><table class="navctrl">\n')
	writeIxCtrl(doc)
	doc.write('\n</table></body></html>')
	doc.close()
}

function writeIxCtrl(doc) {
	var str = new Sarray('<tr>')
	ctrlCount = 1
	if (includeToc) {
		ctrlCount++
		str.add('<th class="nav" width="')
		str.add(widthTocCell)
		str.add('"><a class="navlink" href="javascript:parent.ctrl.useToc()">')
		str.add(ContentsButTxt)
		str.add('</a></th>')
	}
	str.add('<th class="navx" width="')
		str.add(widthIdxCell)
		str.add('">')
		str.add(IndexButTxt)
		str.add('</th>')
	if (includeFts) {
		ctrlCount++
		str.add('<th class="nav" width="')
		str.add(widthFtsCell)
		str.add('"><a class="navlink" href="javascript:parent.ctrl.useSearch()">')
		str.add(SearchButTxt)
		str.add('</a></th>')
	}
	if (includeRel) {
		ctrlCount++
		str.add('<th class="nav" width="')
		str.add(widthRelCell)
		str.add('"><a class="navlink" href="javascript:parent.ctrl.useRel()">')
		str.add(RelatedButTxt)
		str.add('</a></th>')
	}
	str.add('</tr>')
	if (parent.idxExpand && parent.idxOpenCloseButtons) {
		str.add('<tr><td class="navxbtn"')
		if (ctrlCount > 1) {
			str.add(' colspan="')
			str.add(ctrlCount)
			str.add('"')
		}
		str.add('>')
		str.add('<input class="button" type="button" value="')
		str.add(OpenAllButTxt)
		str.add('"\n')
		str.add(' onClick="javascript:parent.ctrl.setIdxGroups(true)"')
		str.add(' alt="')
		str.add(OpenAllButTxt)
		str.add('" />&nbsp;&nbsp;')
		str.add('<input class="button" type="button" value="')
		str.add(CloseAllButTxt)
		str.add('"\n')
		str.add(' onClick="javascript:parent.ctrl.setIdxGroups(false)"')
		str.add(' alt="')
		str.add(CloseAllButTxt)
		str.add('" />')
		str.add('</td></tr>')
	}
	str.add('<tr><td class="navx"')
	if (ctrlCount > 1) {
		str.add(' colspan="')
		str.add(ctrlCount)
		str.add('"')
	}
	str.add('>')
	doc.writeln(str.out())
	if (idxItems.length == 0) {
		doc.writeln(NoIdxMsg)
	} else {
		writeIxList(doc)
	}
	doc.writeln('</td></tr>')
}


function writeIxList(doc) {
	var dat = new Array()
	var curr = ""
	var str = new Sarray()

	if (firstIdxUse) {
		setIxList()
		if (currIxId == "0") {  // starting, set to first entry
			currIxId = ixIdList[0]
		}
		firstIdxUse = false
	}

	for (var i = 0 ; i < ixIdList.length ; i++) {
		curr = ixIdList[i]
		if (curr == currIxId) {
			str.add('<span class="currix">')
		} else {
			str.add('<a href="javascript:parent.ctrl.setIxId')
			str.add("('")
			str.add(curr)
			str.add("')")
			str.add('">')
		}
		str.add(curr)
		if (curr == currIxId) {
			str.add('</span>')
		} else {
			str.add('</a>')
		}
		doc.writeln(str.out())
		str.clear()
	}
}

function setIxList()  {
	var dat = new Array()
	var curr = ""
	var last = ""
	var pos = 0

	ixList = new Array()
	ixIdList = new Array()
	ixIDs = new Array()

	for (var num = 0 ; num < idxItems.length ; num++) {
		dat = idxItems[num]
		if (dat.length == 1) { // start of group
			curr = dat[0]
			if (curr == '!') {
				curr = SymTxt
			} else if (curr == '0') {
				curr = NumTxt
			}
			ixList[pos] = num + 1
			ixIdList[pos] = curr
			ixIDs[curr] = pos
			pos++
		}
	}
}

function getIxID(str) {
	var pos = 0
	var ch = ""
	var start = true

	while (pos < str.length) {
		ch = str.charAt(pos).toUpperCase()
		if (ch == '&') {
			start = false
			while ((pos < str.length) && (ch != ';')) {
				pos++
				ch = str.charAt(pos).toUpperCase()
			}
		} else if (start && (parent.ignoreLeadCharsIX.indexOf(ch) != -1)) {
			pos++
		} else if (parent.ignoreCharsIX.indexOf(ch) != -1) {
			start = false
			pos++
		} else if ((ch >= "0") && (ch <= "9")) {
			return NumTxt
		} else if ((ch < "A") || (ch > "Z")) {
			return SymTxt
		} else {
			return ch
		}
	}
	return "0"
}

function writeIxDocFile(items) {
	//if ((currIxId == lastDocIxId)
	// && (lastNavDoc == 1)
	// && (lastIxShow == lastDocIxShow)
	// && !items) {
	//	return
	//}
	lastNavDoc = 1
	lastDocIxId = items ? -1 : currIxId
	var doc = parent.nav.document
	doc.open()
	doc.write(parent.docHeader)
	doc.write('<head><title>OmniHelp Index File, OHver 0.8</title>\n')
	parent.ctrlCSS(doc)
	doc.writeln('</head>\n<body class="nav" >\n')
	if (items) {
		writeSelIdxItems(doc, items)
	} else {
		writeidxItems(doc)
	}
	doc.write('\n</body></html>')
	doc.close()
	lastDocIxShow = lastIxShow
	if (!idxScroll && (idxExpPos == -1)) {
		setTimeout("parent.nav.scrollTo(0,0)", 1000)
	}
	else {
		currIdxDoc = doc
		setTimeout('idxTimeLoaded()', 1)
	}
}

function idxTimeLoaded() {
	idxLoaded(currIdxDoc)
}

function idxLoaded(doc) {
	if (idxScroll || (idxExpPos > -1)) {
		var anch = 0
		if (idxScroll && parent.isN4) {
			for (anch = 0 ; anch < doc.anchors.length ; anch++) {
				if (unescape(doc.anchors[anch].name) == idxScroll) {
					break
				}
			}
			if (anch >= doc.anchors.length) {
				return
			}
		}
		setDocScrollPos(doc, anch, idxScroll)
		idxScroll = ""
	}
}


function setIdxGroups(open) {
	if (open) {
		idxSetGroupsOpen = true
		idxSetGroupsClosed = false
	} else {
		idxSetGroupsOpen = false
		idxSetGroupsClosed = true
	}
	lastDocIxShow = 2
	if (parent.isNav) {
		idxExpPos = parent.nav.pageYOffset
	} else {
		idxExpPos = parent.nav.document.body.scrollTop
	}
	idxScroll = ""
	writeIxDocFile()
	idxSetGroupsOpen = false
	idxSetGroupsClosed = false
}

function setIxExp(val) {
	var dat = idxItems[val]
	if (dat[3] == 1) {
		dat[3] = 5
	} else if (dat[3] == 5) {
		dat[3] = 1
	}
	lastDocIxShow = 2
	if (parent.isNav) {
		idxExpPos = parent.nav.pageYOffset
	} else {
		idxExpPos = parent.nav.document.body.scrollTop
	}
	idxScroll = ""
	writeIxDocFile()
}

function makeIxID(ltr, str) {
	var re = /[^a-zA-Z0-9\._]/g
	str = escape(str)
	str = str.replace(re, "")
	return ltr + str
}

function writeidxItems(doc) {
	if ((idxItems.length == 0)
	 || (ixIDs.length <= currIxId)
	 || (ixList.length <= ixIDs[currIxId])
	 || (idxItems.length <= (ixList[ixIDs[currIxId]]))) {
		doc.write('<p class="toc1">', NoIdxItemMsg, '<p>')
		return
	}

	var pos = ixIDs[currIxId]
	var spos = ixList[pos]
	var epos = ((pos + 1) < ixList.length) ? ixList[pos + 1] : idxItems.length

	var i = 0
	var ixico = parent.idxIcoBase
	var vis = true
	var iDat = idxItems[0]
	var nDat = idxItems[1]
	var iLev = 1
	var nLev = 1
	var firstLev = 1
	var levList = new Array()
	var ixdef = parent.idxGroupsOpen ? 1 : 5
	var ixlev = parent.idxOpenLevel

	var seePos = 0
	var seeTerm = parent.idxSeeTerm
	var seeAlsoTerm = parent.idxSeeAlsoTerm


	function writeOneIxItem(doc, num) {
		var dat = idxItems[num]
		var top = (num == spos) ? true : false
		var tnum = 0
		var ico = 12
		var lev = dat[0]
		var clev = 1
		var title = ""

		if (!checkCondIx(dat)) {
			return
		}
		if (dat.length == 1) {  // end of group
			return
		}
		var str = new Sarray('<p class="toc')
		str.add(parent.idxExpand ? 'v' : lev)
		str.add('" title="')
		if (dat[2] == -1) {
			str.add(dat[1])
		} else {
			if (dat[1].charAt(0) == "#") {
				str.add(dat[2])
			} else {
				tnum = dat[2]
				str.add(tocItems[tnum][1])
			}
		}
		str.add('">')

		if (parent.idxExpand) {
			if (lev > firstLev) {
				for (clev = firstLev; clev < lev; clev++) {
					str.add('<img src="')
					str.add(ixico)
					str.add(levList[clev])
					str.add('.gif" class="tcv" alt=""/>')
				}
			}
			ico = dat[3] + dat[4]
			if (top && (ico < 11)) {  // top item in ix part
				ico += 2
			}
			if (ico < 9) {
				str.add('<a href="javascript:parent.ctrl.setIxExp(')
				str.add(num)
				str.add(')">')
			}
			str.add('<img src="')
			str.add(ixico)
			str.add(ico)
			str.add('.gif" class="tcv" alt="')
			str.add((ico < 5) ? 'minus' : ((ico < 9) ? 'plus' : ''))
			str.add('"/>')
			if (ico < 9) {
				str.add('</a>')
			}
		}

		if (dat[2] == -1) {  // a heading
			if (dat[0] == 1) {    // top level, set ID for See links
				var src = dat[1]
				var slen = dat[1].indexOf(',')
				if (slen > 1) {
					src = dat[1].substr(0, slen)
				}
				src = makeIxID(currIxId, src)
				str.add('<a id="')
				str.add(src)
				str.add('" name="')
				str.add(src)
				str.add('"></a>')
			}
			title = dat[1]
			if ((seePos = title.indexOf(seeAlsoTerm)) != -1) {
				if (seePos > 0) {
					str.add(title.substr(0, seePos))
				}
				str.add('<i>')
				str.add(seeAlsoTerm)
				str.add('</i>')
				str.add(title.substr(seePos + seeAlsoTerm.length))
			} else if ((seePos = title.indexOf(seeTerm)) != -1) {
				if (seePos > 0) {
					str.add(title.substr(0, seePos))
				}
				str.add('<i>')
				str.add(seeTerm)
				str.add('</i>')
				str.add(title.substr(seePos + seeTerm.length))
			} else {
				str.add(title)
			}
		} else {
			str.add('<a href="')
			if (dat[1].charAt(0) == "#") {  // for See and See Also entries
				var newId = getIxID(dat[2])
				var targ = dat[1].substr(1)
				var len = dat[1].indexOf(',')
				if (len > 1) {
					targ = dat[1].substr(1, len - 1)
				}
				targ = makeIxID(newId, targ)
				str.add('javascript:parent.ctrl.setIdx(\'')
				str.add(newId)
				str.add('\',\'')
				str.add(targ)
				str.add('\')">')
				str.add(dat[2])
			} else {
				str.add('javascript:parent.ctrl.setDoc(')
				str.add(tnum)
				str.add(',\'')
				str.add(dat[1])
				str.add('\')">')
				str.add(tocItems[tnum][1])
			}
			str.add('</a>')
		}
		str.add('</p>')
		doc.writeln(str.out())
	}

	if (parent.idxExpand) {  // expando, write visible nodes

		// if expandable (has subs), show image + if closed and - if open; else |.
		// do not show subs if closed; start with all closed 
		for (i = 0; i < 10; i++) {
			levList[i] = -1
		}

		for (i = ixList[pos]; i < epos; i++) {
			iDat = idxItems[i]
			if (iDat.length == 1) {
				continue
			}
			if (iDat.length == 2) {
				iDat[2] = -1
			}
			if (iDat.length == 3) {
				iDat[3] = 0
			}
			iDat[4] = 0
			iLev = iDat[0]
			if ((i + 1) < idxItems.length) {
				nDat = idxItems[i + 1]
				nLev = nDat[0]
			} else {
				nLev = 0
			}
			levList[iLev] = i
			if (nLev > iLev) {  // sub so this is group
				if (idxSetGroupsOpen) {
					iDat[3] = 1
				} else if (idxSetGroupsClosed) {
					iDat[3] = 5
				} else if (!iDat[3]) {  // never set, use default
					iDat[3] = ixlev ? ((iLev <= ixlev) ? 1 : 5) : ixdef
				}
			} else if (nLev < iLev) { // ending sub
				iDat[3] = 9
				iDat[4] = 1
				while (--iLev > nLev) {
					if (levList[iLev] > -1) {
						idxItems[levList[iLev]][4] = 1
						levList[iLev] = -1
					}
				}
			} else { // same level as next, not a group
				iDat[3] = 9
			}
			idxItems[i] = iDat
		}

		i = epos - 1   // last item in list
		iDat = idxItems[i]
		if (iDat.length == 1) {
			iDat = idxItems[--i]
		}
		iLev = iDat[0]
		levList[iLev] = i
		if (iLev == 1) { // top level
			iDat[3] = 12
			iDat[4] = 0
		} else {
			iDat[3] = 9
			iDat[4] = 1
			while (--iLev > 0) {
				if (levList[iLev] > -1) {
					idxItems[levList[iLev]][4] = 1
					levList[iLev] = -1
				}
			}
		}
		idxItems[i] = iDat

		// do the actual writing of visible items
		vis = true
		nLev = 0
		for (i = 0; i < 10; i++) {
			levList[i] = 13
		}
		for (i = spos; i < epos; i++) {
			iDat = idxItems[i]
			if (iDat.length == 1) {
				break
			}
			if (iDat[0] <= nLev) {  // invisible group ended
				vis = true
				nLev = 0
			}
			if (vis) {
				writeOneIxItem(doc, i)
				if (iDat[4] == 1) {       // corner, last of level
					levList[iDat[0]] = 12    // below it is empty
				} else if (iDat[3] < 9) { // beginning of group
					levList[iDat[0]] = 13    // below it is vert line
				}
				if (iDat[3] == 5) {  // invisible group starting
					nLev = iDat[0]
					vis = false
				}
			}
		}
	} else {  // write everything
		for (var i = spos ; i < epos ; i++) {
			writeOneIxItem(doc, i)
		}
	}

	doc.write('<a id="x00" name="x00"></a>')
}




// processing for KLinks

function writeSelIdxItems(doc, items) {
	var i = 0
	var lname = ""
	var klist = new Array()

	// split up item list at semis
	var ilist = items.split(";")
	for (i = 0 ; i < ilist.length ; i++) {
		lname = ilist[i].toLowerCase()
		ilist[i] = lname
	}

	// sort items (to allow one pass through index)
	ilist.sort(comp)

	// for each item, look for matching text item for all levels
	var state = 1
	var ipos = 0
	var iname = ilist[ipos]
	var xname = ""
	var npos = 0
	var reflev = 0
	var ch = ''
	var num = 0
	var spos = 0

	for (num = 0 ; num < idxItems.length ; num++) {
		dat = idxItems[num]
		if (dat.length == 1) {
			continue
		}
		xname = dat[1].toLowerCase()
		switch (state) {
			case 1: // scanning for first-level match
				if (dat[0] == 1) {
					while (iname < xname) {
						ipos++
						if (ipos >= ilist.length) {
							break
						} else {
							iname = ilist[ipos]
						}
					}
					spos = num - 1
					if (iname.indexOf(xname) == 0) {  // matched at start
						reflev = 2
						if (iname.length == xname.length) {  // full match
							state = 3
						} else {  // matched start, look for rest
							state = 2
							npos = xname.length
							ch = iname.charAt(npos)
							if ((ch == ',') || (ch == ':')) {
								npos++
							}
							ch = iname.charAt(npos)
							if (ch == ' ') {
								npos++
							}
						}
					}
				}
				break
			case 2: // scanning for lower-level match
				if (dat[0] < reflev) {  // failed to find sub
					state = 1
					// advance to next iname
					ipos++
					// go back to last first-level item to rescan for next iname
					num = spos
				}
				if ((dat[0] == reflev) && (dat[2] == -1)) {
					if (iname.indexOf(xname, npos) == npos) {  // matched at start
						reflev++
						if ((iname.length - npos) == xname.length) {  // full match
							state = 3
						} else {  // matched start, keep looking for rest
							npos += xname.length
							ch = iname.charAt(npos)
							if ((ch == ',') || (ch == ':')) {
								npos++
							}
						}
					}
				}
				break
			case 3: // store copies of refs in klist
				if ((dat[0] >= reflev) && (dat[2] > -1)) {
					klist[klist.length] = num
				} else if (dat[0] < reflev) {
					ipos++
					state = 1
					num = spos
				}
				break
			default:
				break
		}
		if (ipos >= ilist.length) {
			break
		} else {
			iname = ilist[ipos]
		}
	}

	var hash = ""

	if (klist.length) {
		// sort klist by topic numbers
		klist.sort(ixncomp)

		// put out list excluding duplicates and current topic
		for (i = 0; i < klist.length ; i++) {
			// will miss non-adjacent dups, needs slice and sort by dat[1]
			dat = idxItems[klist[i]]
			xname = dat[1].toLowerCase()
			if ((hash != xname) && (currTocId != dat[2])) {
				hash = xname
				writeSelIxItem(doc, dat)
			}
		}
	} else {
		doc.write('<p class="toc1">', NoKLinkItemMsg, '<p>')
	}
}

function comp(a, b) {
	if (a < b) {
		return -1
	}
	if (a > b) {
		return 1
	}
	return 0
}

function ixncomp(ax, bx) {
	a = idxItems[ax][2]
	b = idxItems[bx][2]
	return (a - b)
}

function ixscomp(ax, bx) {
	a = idxItems[ax][1]
	b = idxItems[bx][1]
	if (a < b) {
		return -1
	}
	if (a > b) {
		return 1
	}
	return 0
}


function writeSelIxItem(doc, dat) {
	var num = dat[2]
	var str = new Sarray('<p class="toc1" title="')
	str.add(tocItems[num][1])
	str.add('"><a href="javascript:parent.ctrl.setDoc(')
	str.add(num)
	str.add(',\'')
	str.add(dat[1])
	str.add('\')">\n')
	str.add(tocItems[num][1])
	str.add('</a></p>')
	doc.writeln(str.out())
}



// end of ohidx.js

