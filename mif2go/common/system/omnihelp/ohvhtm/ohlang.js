// OmniHelp message text for frameset, US English version, OHver 0.8
// Copyright (c) 2006 by Jeremy H. Griffith.  All rights reserved.

var useXhtml = false

// ohstart.js
var LoadingMsg = 'OmniHelp loading...'
var NoFramesetMsg = 'Sorry, cannot open new frameset window.'

// ohframe.js
var NoFramesMsg = 'Sorry, OmniHelp requires a browser that supports frames.'

var frameDocName = "ohframe.htm"
var ctrlName = "ohctrl.htm"
var topNavName = "ohtop.htm"
var navCtrlName = "ohnavctrl.htm"
var navDocName = "ohnav.htm"
var mainDocName = "ohmain.htm"
var mergeDocName = "ohmerged.htm"
var blankName = "javascript:parent.blank()"

var docHeader1 = '<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">\n'
var docHeader2 = '<html lang="en">\n'
var docHeader = docHeader1 + docHeader2

// see also ohlangtp.js for top nav, and ohlangct.js for ctrl

// end of ohlang.js for US English

