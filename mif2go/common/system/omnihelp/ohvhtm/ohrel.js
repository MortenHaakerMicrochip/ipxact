
// OmniHelp ctrl functions for related (ALinks), OHver 0.8
// Copyright (c) 2005 by Jeremy H. Griffith.  All rights reserved.

var lastRelTocId = -1
var lastRelType = -1
var relCtrlHigh = 30

function useRel() {
	currNavCtrl = 3
	relType = 1
	writeRelCtrlFile()
	writeRelDocFile()
	setCookies()
}

function writeRelCtrlFile(subjects) {
	if ((lastNavCtrl == 3) && (lastRelType == relType)
	 && (lastRelTocId == currTocId) && !subjects) {
		return
	}

	if (parent.relShowSubjects) {
		relCtrlHigh = 120
	} 
	var str = new Sarray()
	var obj = parent.isIE4 ? parent.document.all["leftNavCtrl"] : parent.document.getElementById("leftNavCtrl")
	str.add(relCtrlHigh)
	str.add(', *')
	obj.rows = str.out()

	lastNavCtrl = 3
	lastRelType = (relType != 1) ? -1 : 1
	var doc = parent.navctrl.document
	doc.open()
	doc.write(parent.docHeader)
	doc.write('<head><title>OmniHelp Nav Control: Related, OHver 0.8</title>\n')
	parent.ctrlCSS(doc)
	doc.write('</head>\n<body class="navctrl"><table class="navctrl">\n')
	writeALinkCtrl(doc, subjects)
	doc.write('\n</table></body></html>')
	doc.close()
	relType = 1
}

function writeALinkCtrl(doc, subjects) {
	var i = 0
	var line = 0
	var sparray = new Array()
	var relLinks = new Array()
	var str = new Sarray('<tr>')

	ctrlCount = 1
	if (includeToc) {
		ctrlCount++
		str.add('<th class="nav" width="')
		str.add(widthTocCell)
		str.add('"><a class="navlink" href="javascript:parent.ctrl.useToc()">')
		str.add(ContentsButTxt)
		str.add('</a></th>\n')
	}
	if (includeIdx) {
		ctrlCount++
		str.add('<th class="nav" width="')
		str.add(widthIdxCell)
		str.add('"><a class="navlink" href="javascript:parent.ctrl.useIx()">')
		str.add(IndexButTxt)
		str.add('</a></th>\n')
	}
	if (includeFts) {
		ctrlCount++
		str.add('<th class="nav" width="')
		str.add(widthFtsCell)
		str.add('"><a class="navlink" href="javascript:parent.ctrl.useSearch()">')
		str.add(SearchButTxt)
		str.add('</a></th>\n')
	}
	str.add('<th class="navx" width="')
	str.add(widthRelCell)
	str.add('">')
	str.add(RelatedButTxt)
	str.add('</th></tr>\n')
	if (parent.relShowSubjects) {
		str.add('<tr><td class="navx"')
		if (ctrlCount > 1) {
			str.add('colspan="')
			str.add(ctrlCount)
			str.add('"')
		}
		str.add('><p class="navx">')
		str.add((relType < 3) ? RelSubjMsg : RelIdxMsg)
		str.add('<br />')
		if (relType == 1) {
			relLinks.length = 0
			relLinks = hasRelLinks(currTocId).split(';')
		}
		if (subjects) {  // ALink or KLink jump
			sparray = subjects.split(";")
			for (i = 0 ; i < sparray.length ; i++) {
				str.add(sparray[i])
				if (i < (sparray.length - 1))
					str.add(' &#x2022; ')
			}
		} else if ((relType == 1) && (relLinks.length > 0)) {
			for (i = 0 ; i < relLinks.length ; i++) {
				str.add(relLinks[i])
				if (i < (relLinks.length - 1))
					str.add(' &#x2022; ')
			}
		} else {
			str.add('None')
		}
		str.add('</p></td></tr>\n')
	}
	doc.write(str.out())
}

function hasRelLinks(id) {
	var itemNum = 0
	var relItemDat = new Array()
	var topicItems = new Array()
	var idx = 0
	var relLinkList = new Array()

	relLinkList.length = 0
	for (itemNum = 0 ; itemNum < relItems.length ; itemNum++ ) {
		relItemDat = relItems[itemNum]
		topicItems = relItemDat[1]
		for (idx = 0 ; idx < topicItems.length ; idx++) {
			if (topicItems[idx] == id) {
				relLinkList[relLinkList.length] = relItemDat[0]
				break
			}
		}
	}
	return relLinkList.join(';')
}

function writeRelDocFile(subjects) {
	if ((lastNavDoc == 3) && (lastRelTocId == currTocId) && !subjects) {
		return
	}
	lastNavDoc = 3
	lastRelTocId = subjects ? -1 : currTocId
	var doc = parent.nav.document
	doc.open()
	doc.write(parent.docHeader)
	doc.write('<head><title>OmniHelp Related Topics File, OHver 0.8</title>\n')
	parent.ctrlCSS(doc)
	doc.write('</head>\n<body class="nav">\n')

	doc.write('<p class="navx">')
	if (relType == 1) {
		doc.write(RelMsg)
	} else if (relType == 2) {  // ALink jump info
		doc.write(ALinkMsg)
	} else if (relType == 3) {  // KLink jump info
		doc.write(KLinkMsg)
	}
	doc.write('</p>\n')

	if (currTocId == -1) {
		doc.write('<p class="rel">', DocNoRelMsg, '</p>')
	} else {
		writeALinks(doc, subjects)
	}
	doc.write('\n</body></html>')
	doc.close()
	setTimeout("parent.nav.scrollTo(0,0)", 1000)
}

function numcomp(a, b) {
	return a - b
}

function writeALinks(doc, subjects) {
	var itemNum = 0
	var relItemDat = new Array()
	var itemSel = currTocId
	var topicItems = new Array()
	var idx = 0
	var idx2 = 0
	var relTopics = new Array()
	var relPos = 0
	var relName = ""
	var sName = ""
	var subPos = 0
	var ePos = 0

	if (subjects) {  // ALink jump list
		sName = subjects.toLowerCase()
		for (itemNum = 0 ; itemNum < relItems.length ; itemNum++ ) {
			relItemDat = relItems[itemNum]
			relName = relItemDat[0].toLowerCase()
			subPos = sName.indexOf(relName)
			ePos = subPos + relName.length
			if ((subPos > -1)
			 && ((subPos == 0) || (sName.charAt(subPos - 1) == ";"))
			 && ((ePos == sName.length) || (sName.charAt(ePos) == ";"))) {
				topicItems = relItemDat[1]
				for (idx = 0 ; idx < topicItems.length ; idx++) {
					if (topicItems[idx] != itemSel) {
						relTopics[relTopics.length] = topicItems[idx]
					}
				}
			}
		}
	} else {
	// go through all alink sets for those that ref the current topic
	// add all members of those sets to relTopics (except current topic)
		for (itemNum = 0 ; itemNum < relItems.length ; itemNum++ ) {
			relItemDat = relItems[itemNum]
			topicItems = relItemDat[1]
			for (idx = 0 ; idx < topicItems.length ; idx++) {
				if (topicItems[idx] == itemSel) {
					for (idx2 = 0 ; idx2 < topicItems.length ; idx2++) {
						if (topicItems[idx2] != itemSel) {
							relTopics[relTopics.length] = topicItems[idx2]
						}
					}
					break
				}
			}
		}
	}

	var lastItem = -1

	if (relTopics.length > 0) {
		relTopics.sort(numcomp)
		for (relPos = 0; relPos < relTopics.length ; relPos++) {
			itemNum = relTopics[relPos]
			if (itemNum != lastItem) {
				writeOneRelItem(doc, itemNum)
				lastItem = itemNum
			}
		}
	} else {
		doc.write('<p class="rel">', NoRelItemMsg, '</p>')
	}
}


function writeOneRelItem(doc, num) {
	var str = new Sarray('<p class="rel" title="')
	str.add(tocItems[num][1])
	str.add('"><a href="javascript:parent.ctrl.setDoc(')
	str.add(num)
	str.add(')">')
	str.add(tocItems[num][1])
	str.add('</a></p>')
	doc.writeln(str.out())
}


// end of relcode.js
