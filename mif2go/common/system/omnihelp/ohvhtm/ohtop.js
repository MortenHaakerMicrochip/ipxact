// OmniHelp functions for top nav table construction, OHver 0.8
// Copyright (c) 2006 by Jeremy H. Griffith.  All rights reserved.

function writeTopNavTable(doc) {
	var str = new Sarray()

	function writeButton(id, name, title) {
		if (parent.topButtons) {
			str.add('<button type="button" id="')
			str.add(id)
			str.add('"\n')
			str.add(' onclick="parent.ctrl.get')
			str.add(id.substr(3))
			str.add('()"\n')
			str.add(' title="')
			str.add(title)
			str.add('">')
			str.add(name)
			str.add('</button>\n')
		} else {
			str.add('<a id="')
			str.add(id)
			str.add('" href="javascript:parent.ctrl.get')
			str.add(id.substr(3))
			str.add('()" title="')
			str.add(title)
			str.add('">')
			str.add(name)
			str.add('</a>\n')
		}
	}

	function writeButtonCell(id1, name1, title1, wid, id2, name2, title2) {
		str.add('<td class="topnav')
		if (wid > 0) {
			str.add('" width="')
			str.add(wid)
		}
		str.add('">')
		writeButton(id1, name1, title1)
		if (name2) {
			str.add('&nbsp;')
			writeButton(id2, name2, title2)
		}
		str.add('</td>\n')
	}

	str.add('<table class="topnav" border="0" height="')
	str.add(parent.topHigh)
	str.add('" width="100%"><tr>\n')
	if (parent.topFirst) {
		str.add('<td class="topnav" width="')
		str.add(parent.leftWide)
		str.add('">\n')
		if (parent.topLeft) {
			str.add(parent.topLeft)
		} else {
			str.add('Your Company Name<br />and Logo Here')
		}
		str.add('</td>\n')
	}
	if (parent.useStart) {
		writeButtonCell("topStart", StartButTxt, StartButTitle, 0)
	}
	if (parent.usePrevNext) {
		writeButtonCell("topPrev", PrevButTxt, PrevButTitle, 0, "topNext", NextButTxt, NextButTitle)
	}
	if (parent.useBackForward) {
		writeButtonCell("topBack", BackButTxt, BackButTitle, 0, "topFwd", FwdButTxt, FwdButTitle)
	}
	if (parent.useHideShow) {
		writeButtonCell("topHS", (parent.showNavLeft) ? HideButTxt : ShowButTxt, (parent.showNavLeft) ? HideButTitle : ShowButTitle, 0)
	}
	str.add('\n')
	str.add('<td class="topnav">')
	if (parent.topRight) {
		str.add(parent.topRight)
	} else {
		str.add('<img src="')
		str.add(parent.validImg)
		str.add('" border="0"\n alt="')
		str.add(parent.validAlt)
		str.add('" height="25" width="71" />')
	}
	str.add('</td></tr></table>\n')
	doc.write(str.out())
	parent.topNavDoc = doc
}


// functions for fast string concatenation

function Sarray(value) {
	this.sarr = new Array("")
	if (value) {
		this.add(value)
	}
}

Sarray.prototype.add = function(value) {
	if (value == 0) {
		this.sarr.push('' + value)
	} else {
		this.sarr.push(value)
	}
}

Sarray.prototype.clear = function() {
	this.sarr.length = 1
}

Sarray.prototype.out = function() {
	return this.sarr.join("")
}


// end of ohtop.js


