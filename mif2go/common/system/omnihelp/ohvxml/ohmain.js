
// OmniHelp JS code referenced in head of all content files, OHver 0.9
// Copyright (c) 2007 by Jeremy H. Griffith.  All rights reserved.

if (opener) {
	opener.parent.mainCSS(document)
} else if (parent) {
	parent.mainCSS(document)
	parent.ctrl.mainChanged()
}

function doSection(id){
	if (parent) {
		parent.ctrl.doSection(document, id)
	}
}

function noSection(id){
	if (parent) {
		parent.ctrl.noSection(document, id)
	}
}

function sec(item, wide, high, param) {
	if (!opener) {
		parent.ctrl.secWin(item, wide, high, param)
	}
}

function alink(str) {
	if (parent) {
		parent.ctrl.alink(str)
	}
}

function klink(str) {
	if (parent) {
		parent.ctrl.klink(str)
	}
}

// end of ohmain.js
