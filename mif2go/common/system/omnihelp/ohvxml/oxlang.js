// OmniHelp message text for frameset, US English version, OHver 0.8
// Copyright (c) 2006 by Jeremy H. Griffith.  All rights reserved.

var useXhtml = true

// ohstart.js
var LoadingMsg = 'OmniHelp XHTML loading...'
var NoFramesetMsg = 'Sorry, cannot open new frameset window.'

// ohframe.js
var NoFramesMsg = 'Sorry, OmniHelp requires a browser that supports frames.'

var frameDocName = "oxframe.htm"
var ctrlName = "oxctrl.htm"
var topNavName = "oxtop.htm"
var navCtrlName = "oxnavctrl.htm"
var navDocName = "oxnav.htm"
var mainDocName = "oxmain.htm"
var mergeDocName = "oxmerged.htm"
var blankName = "javascript:parent.blank()"

// <?xml version="1.0" encoding="UTF-8" ?>
var docHeader1 = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"\n'
var docHeader2 = '"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">\n'
var docHeader3 = '<html xmlns="http://www.w3.org/1999/xhtml" lang="en">\n'
var docHeader = docHeader1 + docHeader2 + docHeader3

// see also ohlangtp.js for top nav, and ohlangct.js for ctrl

// end of ohlang.js for US English

