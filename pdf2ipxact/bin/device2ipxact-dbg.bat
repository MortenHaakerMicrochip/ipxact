@echo off
SET device=%1
SET dospath=..\data\dos_xmls\%device%\%device%
SET xmlpath=..\ipxact_lib\microchip\device\%device%\REV\%device%

echo DOS input: %dospath%.xml
echo IPX output: %xmlpath%_debug.REV.xml
echo Logged messages: %xmlpath%_debug.REV.log

echo java -agentlib:jdwp=transport=dt_socket,server=y,address=8888,suspend=%2
echo    -jar %~dp0\PDF2IPXACT.jar 
echo    -reg 
echo    -dos %dospath%_debug.xml  
echo    -output %xmlpath%_debug.REV.xml 
echo    -log %xmlpath%_debug.REV.log
java -jar -agentlib:jdwp=transport=dt_socket,server=y,address=8888,suspend=%2 %~dp0\PDF2IPXACT.jar -reg -dos %dospath%.xml -output %xmlpath%_debug.REV.xml -log %xmlpath%_debug.REV.log

