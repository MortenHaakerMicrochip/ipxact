@echo off
SET device=%1
SET dospath=..\data\dos_xmls\%device%\%device%
SET xmlpath=..\ipxact_lib\microchip\device\%device%\REV\%device%

echo DOS input: %dospath%.xml
echo IPX output: %xmlpath%.REV.xml
echo Logged messages: %xmlpath%.REV.log

echo java -jar %~dp0\PDF2IPXACT.jar 
echo    -reg 
echo    -dos %dospath%.xml  
echo    -output %xmlpath%.REV.xml 
echo    -log %xmlpath%.REV.log
java -jar %~dp0\PDF2IPXACT.jar -reg -dos %dospath%.xml -output %xmlpath%.REV.xml -log %xmlpath%.REV.log

