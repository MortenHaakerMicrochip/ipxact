﻿/*
 * Program Name:                                                     
 *    frame2xml.jsx
 *                                                                   
 * General Description:                                              
 *    save a book as XML
 *                                                                   
 *********************************************************************/      
     
//Initialising Current Component
 
#target framemaker



//this is to suppress the paragraph tag warnings
Notification(Constants.FA_Note_Alert,true) //Registering for the event  FA_Note_Alert
Notification(Constants.FA_Note_PreSaveXML,true)
Notification(Constants.FA_Note_PostSaveXML,true)
Notification(Constants.FA_Note_BackToUser,true)
$.writeln("Script Start");
//alert("TEST ALERT");

var argpath = String((new File($.fileName)).parent)+"/frame2xml.csv";
var args = readArgs(argpath);

for(var csvIndex=0; csvIndex < args.length; csvIndex++){

    var fmBook = openfile(args[csvIndex][0]); 
    var currBook = app.ActiveBook;     
    var bookComp=currBook.FirstComponentInBook;
    var compId = bookComp.id;
    var outpath = args[csvIndex][1]; //OUTPATH MUST BE AN ABSOLUTE PATH!


    //ConvertAllFM12 (compId)
    SaveAsXML(currBook,outpath); //saving as XML     
    //SaveAsXML(currBook,"C:\\dos\\macro\\bus\\bus_matrix_bvci_v3 Rev A\\bus_matrix_bvci_v3.xml"); //saving as XML     
    $.writeln("Outputing: " + outpath)
         
 }

Notification(Constants.FA_Note_Alert,false) //Registering for the event  FA_Note_Alert
Notification(Constants.FA_Note_PreSaveXML,false)
Notification(Constants.FA_Note_PostSaveXML,false)
Notification(Constants.FA_Note_BackToUser,false)
$.writeln("Complete, no errors!")



function Notify(note, object, sparam, iparam) {
    
    
    alert("TEST ALERT");
    $.writeln("NOTIFY CALL");

    switch(note) {

        case Constants.FA_Note_Alert : 
            //if (sparam.match(/.*Paragraph Catalog.*/g)) {
                ReturnValue(Constants.FR_YesOperation)
            //}
        break;     
            
    }
}


function readArgs (thePath) {

    var returnArray = [];
    
    if (File(thePath).exists == true) {

        var file = File(thePath);
        file.open("r");
        file.encoding= 'BINARY';
        while(!file.eof){
            returnArray.push(file.readln().split(","));
        }
        file.close();
        return returnArray;

    }
};
 
 function ConvertAllFM12(compId) {
     //Loop for traversing hierarchy of book
     while(compId)
     {   
        var nextId = 0;
        var parentId = 0;
        var compType=bookComp.ComponentType;

        //Assigning next component - If current component  is Group or Folder(move downward in hierarchy)
        if(Constants.FV_BK_FOLDER == compType || Constants.FV_BK_GROUP == compType)
        {  
                var nextComp=bookComp.FirstComponentInBookComponent;
                nextId = nextComp.id;
        }
        //Assigning next component - If current component is not a Group or Folder i.e. a file
        if(nextId == 0)
        {     
            var nextComp = bookComp.NextComponentInBook;
           nextId = nextComp.id;
         }  		 
        //Initialising parent component
        var parentComp = bookComp;
        parentId = parentComp.id;
            
        //Moving upward in hierarchy if reached end of group or folder
        while((nextId == 0) && (parentId != 0))
        {
                parentComp = parentComp.BookComponentParent;
                nextComp = parentComp.NextComponentInBook;
                parentId = parentComp.id;
                nextId = nextComp.id;
        }
             
        //Opening the file
        var fname = bookComp.Name;
        fileId = openfile(fname); // Here openfile function is called
        
        bname.SimpleImportFomats(compId,Constants.FF_UFF_PGF);
        
        SaveAsFM12(fileId); //save files 

        //Iterating to next component
        bookComp = nextComp ;
        compId = bookComp.id;
     }//end of loop
}

function openfile(filename)
{
    openProp = GetOpenDefaultParams()

    i=GetPropIndex(openProp,Constants.FS_FileIsOldVersion)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_FontChangedMetric)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_FontNotFoundInCatalog)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_FontNotFoundInDoc)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_LanguageNotAvailable)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_LockCantBeReset)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_UpdateTextReferences)
    openProp[i].propVal.ival=Constants.FV_DoNo
    i=GetPropIndex(openProp,Constants.FS_UpdateXRefs)
    openProp[i].propVal.ival=Constants.FV_DoNo
    i=GetPropIndex(openProp,Constants.FS_UseAutoSaveFile)
    openProp[i].propVal.ival=Constants.FV_DoNo
    i=GetPropIndex(openProp,Constants.FS_UseRecoverFile)
    openProp[i].propVal.ival=Constants.FV_DoNo
    i=GetPropIndex(openProp,Constants.FS_AlertUserAboutFailure)
    openProp[i].propVal.ival=false
    i=GetPropIndex(openProp,Constants.FS_BeefyDoc)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_FileIsInUse) 
    openProp[i].propVal.ival=Constants.FV_OpenViewOnly
    i=GetPropIndex(openProp,Constants.FS_MakeVisible)
    openProp[i].propVal.ival=true
    i=GetPropIndex(openProp,Constants.FS_RefFileNotFound)
    openProp[i].propVal.ival=Constants.FV_AllowAllRefFilesUnFindable
    i=GetPropIndex(openProp,Constants.FS_FileIsOldVersion)
    openProp[i].propVal.ival=Constants.FV_DoOK
    i=GetPropIndex(openProp,Constants.FS_FileIsStructured)
    openProp[i].propVal.ival=Constants.FV_OpenViewOnly
    i=GetPropIndex(openProp,Constants.FS_OpenFileNotWritable)
    openProp[i].propVal.ival=Constants.FV_DoOK

    retParm = new PropVals();
    fileOpen=Open(filename,openProp,retParm);
    
    if (fileOpen.ObjectValid()==0) {
            PrintOpenStatus(retParm);
     }
    
    return fileOpen;
}

function SaveAsXML(file,output) 
{	
    var params = GetSaveDefaultParams();
    var returnParams =new PropVals();
    var i = GetPropIndex(params, Constants.FS_FileType);
    params[i].propVal.ival =Constants.FV_SaveFmtXml;
    

    FA_errno=0;
    outputFile = new File(output);
    outputFolder = outputFile.parent;
    if(!outputFolder.exists){
        outputFolder.create()
    }
    
    var status = file.Save(output,params,returnParams);
    file.Close(Constants.FF_CLOSE_MODIFIED);
    return
}

function SaveAsFM12(file) 
{	
    var params = GetSaveDefaultParams();
    var returnParams =new PropVals();

    file.Save(file.Name,params,returnParams);
    file.Close(Constants.FF_CLOSE_MODIFIED);
    return
}

















