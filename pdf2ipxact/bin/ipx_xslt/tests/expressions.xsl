<?xml version="1.0"?>
<?altova_samplexml C:\edwin\dev\spiritconsortium.org\repository\ipxact\2.0\update_scripts\tests\expressions.xml?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xalan="http://xml.apache.org/xalan" xmlns:exslt="http://exslt.org/common" xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="xalan exslt msxsl">
	<xsl:import href="../convert_expressions-other.xsl"/>
	<xsl:import href="../convert_expressions-exslt.xsl"/>
	<xsl:import href="../convert_expressions-msxsl.xsl"/>
	<xsl:import href="../convert_expressions-xalan.xsl"/>

	<xsl:output method="xml" indent="yes" encoding="UTF-8"/>

	<xsl:template match="*">
		<xsl:copy>
			<xsl:apply-templates/>
		</xsl:copy>
	</xsl:template>

	<xsl:template match="expression">
		<xsl:variable name="result">
			<xsl:choose>
      	<xsl:when test="function-available('msxsl:node-set')">
					<xsl:call-template name="msxsl:parse">
						<xsl:with-param name="value" select="normalize-space(.)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="function-available('exslt:node-set')">
					<xsl:call-template name="exslt:parse">
						<xsl:with-param name="value" select="normalize-space(.)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:when test="function-available('xalan:nodeset')">
					<xsl:call-template name="xalan:parse">
						<xsl:with-param name="value" select="normalize-space(.)"/>
					</xsl:call-template>
				</xsl:when>
				<xsl:otherwise>
					<xsl:call-template name="parse">
						<xsl:with-param name="value" select="normalize-space(.)"/>
					</xsl:call-template>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<expression expected="{@expected}"><xsl:value-of select="normalize-space($result)"/></expression>

		<xsl:if test="normalize-space($result) != normalize-space(@expected)">
			<xsl:message>[ERROR] Result: "<xsl:value-of select="normalize-space($result)"/>" != Expected: "<xsl:value-of select="@expected"/>"</xsl:message>
		</xsl:if>
	</xsl:template>

	<xsl:template match="scale-expression">
		<xsl:variable name="result">
			<xsl:call-template name="parse-scale">
				<xsl:with-param name="value" select="normalize-space(.)"/>
				<xsl:with-param name="bit-length" select="@bit-length"/>
			</xsl:call-template>
		</xsl:variable>
		<scale-expression expected="{@expected}"><xsl:value-of select="normalize-space($result)"/></scale-expression>

		<xsl:if test="normalize-space($result) != normalize-space(@expected)">
			<xsl:message>ERROR: [Result] "<xsl:value-of select="normalize-space($result)"/>" != [Expected] "<xsl:value-of select="@expected"/>"</xsl:message>
		</xsl:if>
	</xsl:template>
</xsl:stylesheet>
