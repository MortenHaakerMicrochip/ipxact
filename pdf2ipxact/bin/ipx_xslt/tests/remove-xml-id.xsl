<xsl:stylesheet version="2.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema">

<xsl:strip-space elements="*"/>
<xsl:output method="xml" indent="yes"/>

<!-- Process the document node. -->
<xsl:template match="/">
  <xsl:apply-templates select="comment() | processing-instruction()"/>
  <xsl:apply-templates select="*"/>
</xsl:template>

<xsl:template match="*">
  <xsl:element name="{name()}" namespace="{namespace-uri()}">
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<!-- Copy comments, pi's and text. -->
<xsl:template match="comment() | processing-instruction()">
  <xsl:copy>
    <xsl:apply-templates/>
  </xsl:copy><xsl:text>
</xsl:text>
</xsl:template>

<xsl:template match="@*">
  <xsl:attribute name="{name()}" namespace="{namespace-uri()}">
    <xsl:value-of select="."/>
  </xsl:attribute>
</xsl:template>

<xsl:template match="/xs:*">
  <xsl:element name="xs:{local-name()}">
    <xsl:for-each select="namespace::*"><xsl:copy/></xsl:for-each>
    <xsl:apply-templates select="@*"/>
    <xsl:apply-templates/>
  </xsl:element>
</xsl:template>

<xsl:template match="xs:attributeGroup[@ref='ipxact:id.att']">
  <!-- remove -->
</xsl:template>

</xsl:stylesheet>