@echo off
SET macro=%1
SET dospath=..\data\dos_xmls\%macro%\%macro%
SET xmlpath=..\ipxact_lib\microchip\ip\%macro%\REV\%macro%

echo DOS input: %dospath%.xml
echo IPX output: %xmlpath%_debug.REV.xml
echo Logged messages: %xmlpath%.REV.log
echo java -jar %~dp0\PDF2IPXACT.jar -dos %dospath%.xml  -output %xmlpath%_debug.REV.xml -ipxact %xmlpath%.REV.xml -log %xmlpath%.REV.log -debug
java -jar -agentlib:jdwp=transport=dt_socket,server=y,address=8888,suspend=%2 %~dp0\PDF2IPXACT.jar -dos %dospath%.xml -output %xmlpath%_debug.REV.xml -ipxact %xmlpath%.REV.xml -log %xmlpath%.REV.log -debug 