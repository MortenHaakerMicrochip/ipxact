@echo off
SET macro=%1
SET dospath=..\data\dos_xmls\%macro%\%macro%
SET xmlpath=..\ipxact_lib\microchip\ip\%macro%\REV\%macro%
SET MERGE=%2

echo DOS input: %dospath%.xml
echo IPX output: %xmlpath%.REV.xml
echo Logged messages: %xmlpath%.REV.log

if "%MERGE%"=="-m" (
    echo java -jar %~dp0\PDF2IPXACT.jar -dos %dospath%.xml  -output %xmlpath%.REV.xml -ipxact %xmlpath%.REV.xml -log %xmlpath%.REV.log
    java -jar %~dp0\PDF2IPXACT.jar -dos %dospath%.xml -output %xmlpath%.REV.xml -ipxact %xmlpath%.REV.xml -log %xmlpath%.REV.log
) else (
    echo java -jar %~dp0\PDF2IPXACT.jar -dos %dospath%.xml  -output %xmlpath%.REV.xml -log %xmlpath%.REV.log
    java -jar %~dp0\PDF2IPXACT.jar -dos %dospath%.xml -output %xmlpath%.REV.xml -log %xmlpath%.REV.log
)
