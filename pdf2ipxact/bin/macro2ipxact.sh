#!/bin/bash
MACRO="$1"
DOSPATH="../data/dos_xmls/$MACRO/$MACRO"
XMLPATH="../ipxact_lib/microchip/ip/$MACRO/REV/$MACRO"

echo $MACRO
echo "$DOSPATH.xml"
echo "$XMLPATH.REV.xml"

java -jar PDF2IPXACT.jar -dos $DOSPATH.xml -output $XMLPATH.REV.xml -ipxact $XMLPATH.REV.xml -log $XMLPATH.REV.log
