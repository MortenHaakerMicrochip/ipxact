<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

  <!-- Sample xsl file for processing the filemap after conversion and before import -->

  <xsl:output method="xml"/>
  
  <!-- find any files that shoudl be resources by looking for the naming convention '-resource' -->
  <xsl:template match="//file[contains(@filename,'-resource.dita')]">

    <xsl:variable name="targetFolder">
      <xsl:call-template name="replace-string">
        <xsl:with-param name="text" select="./@targetfolder"/>
        <xsl:with-param name="replace" select="'topics'"/>
        <xsl:with-param name="with" select="'resources'"/>
      </xsl:call-template>
    </xsl:variable>
    <file>
      <xsl:attribute name="targetfolder"><xsl:value-of select="$targetFolder"/></xsl:attribute>
      <xsl:attribute name="filetype">ISHLibrary</xsl:attribute>
      <xsl:copy-of select="@*[not(name()='targetfolder') and not(name()='filetype')]"/>
      
      <!-- insert existing metadata fields -->
      <xsl:apply-templates/>
    </file>
    
  </xsl:template>
  
  <!-- also replace the file type of the ishobject within the resource file -->
  <xsl:template match="//file[contains(@filename,'-resource.dita')]/ishobject">
    <ishobject>
      <xsl:attribute name="ishtype">ISHLibrary</xsl:attribute>
      <xsl:copy-of select="@*[not(name()='ishtype')]"/>
      
      <!-- insert existing metadata fields -->
      <xsl:apply-templates/>
    </ishobject>
  </xsl:template>



  <xsl:template name="replace-string">
    <xsl:param name="text"/>
    <xsl:param name="replace"/>
    <xsl:param name="with"/>
    <xsl:choose>
      <xsl:when test="contains($text,$replace)">
        <xsl:value-of select="substring-before($text,$replace)"/>
        <xsl:value-of select="$with"/>
        <xsl:call-template name="replace-string">
          <xsl:with-param name="text"
            select="substring-after($text,$replace)"/>
          <xsl:with-param name="replace" select="$replace"/>
          <xsl:with-param name="with" select="$with"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$text"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
  
  <!-- catch all -->
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>