<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" version="2.0">
    <xsl:strip-space elements="*"/>

    <xsl:template match="/" mode="dita-map">
        <xsl:apply-templates mode="dita-map"/>
    </xsl:template>
    
    <!-- identity transform -->
    <xsl:template match="@*|node()" mode="dita-map">
        <xsl:copy exclude-result-prefixes="#all" copy-namespaces="no">
            <xsl:apply-templates select="@*|node()" mode="dita-map"/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="/map/topicref" mode="dita-map">
        <xsl:variable name="href-raw"><xsl:value-of select="@href"/></xsl:variable>
        <xsl:variable name="type"><xsl:value-of select="@type"/></xsl:variable>
        <xsl:variable name="navtitle"><xsl:value-of select="@navtitle"/></xsl:variable>
        <xsl:variable name="href" select="replace($href-raw,'#[\S ]+','')"/>
                
        <xsl:choose>
            <xsl:when test="contains(lower-case($href),'sicover')">
                <topicref href="{$href}" type="{$type}" navtitle="Sicover"/>
            </xsl:when>
            <xsl:when test="contains(lower-case($href),'lastpage')">
            </xsl:when>    
            <xsl:otherwise>
                
                <!-- Per Teresa: Heading1 with text is preferred to empty heading1, 
                    so leave topics in so we can add intro text where it doesn't exist. -->
                <!-- 
                <topichead type="{$type}" navtitle="{$navtitle}">
                    <topicref href="{$href}" type="{$type}" navtitle="{$navtitle}"/> 
                    <xsl:apply-templates select="./node()" mode="dita-map"/>
                </topichead>
                 -->
                <xsl:copy exclude-result-prefixes="#all" copy-namespaces="no">
                    <xsl:apply-templates select="@*|node()" mode="dita-map"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- remove two 'aa#####" blank files from map -->
    <xsl:template match="//topicref[matches(@href,'aa[0-9]+')]" mode="dita-map"/>

    <xsl:template match="//topicref/@href" mode="dita-map">
        <xsl:variable name="href-new" select="replace(.,'#[\S ]+','')"/>
        <xsl:attribute name="href"><xsl:value-of select="$href-new"/></xsl:attribute>
    </xsl:template>
    
    <!-- Insert formatting for HW Interface and Reg Interface to map -->
    <xsl:template match="/map" mode="dita-map">
        <!--
            <xsl:variable name="macroname" select="replace(@id,'[- _]DOS-[0-9]+','')"/>
        -->
        <xsl:variable name="macroname" select="replace(@id,' ','%20')"/>        
        
        <map id="{$macroname}">
            <!-- First get all the current map content -->
            <xsl:apply-templates mode="dita-map"/>
            
            <!-- Then add placeholder pointers to map -->
            <xsl:if test="not(./topicref/@href[contains(.,'hw_interface.dita')])">
                <topicref href="{$macroname}_hw_interface.dita" type="concept">
                    <topicref href="{$macroname}_hw_ports.dita"/>
                    <topicref href="{$macroname}_hw_parameters.dita"/>
                </topicref>
            </xsl:if>
            <xsl:if test="not(./mapref/@href[contains(.,'sidsc.ditamap')])">
                <mapref href="{$macroname}-sidsc.ditamap"/>
            </xsl:if>


        </map>
                
    </xsl:template>

</xsl:stylesheet>
