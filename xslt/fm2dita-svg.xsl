<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:output method="xml" indent="yes"/>

    <!-- identity transform -->
    <xsl:template match="@*|node()" >
        <xsl:copy>
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
    
    <!-- Replace broken "Äô" text in SVG graphics with ' character -->
    <!-- Note two different comma chars: ‚ and , require two different templates... -->
    <xsl:template match="//text()[contains(.,'Äô')]">
        <xsl:variable name="apos">&apos;</xsl:variable>
        <xsl:variable name="newtext">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="." />
                <xsl:with-param name="replace" select="'Äô'" />
                <xsl:with-param name="by" select="$apos" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$newtext"/>
    </xsl:template>
    
    <xsl:template match="//text()[contains(.,'‚Äô')]">
        <xsl:variable name="apos">&apos;</xsl:variable>
        <xsl:variable name="newtext">
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="." />
                <xsl:with-param name="replace" select="'‚Äô'" />
                <xsl:with-param name="by" select="$apos" />
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$newtext"/>
    </xsl:template>
    
    
    <xsl:template name="string-replace-all">
        <xsl:param name="text" />
        <xsl:param name="replace" />
        <xsl:param name="by" />
        <xsl:choose>
            <xsl:when test="$text = '' or $replace = ''or not($replace)" >
                <!-- Prevent this routine from hanging -->
                <xsl:value-of select="$text" />
            </xsl:when>
            <xsl:when test="contains($text, $replace)">
                <xsl:value-of select="substring-before($text,$replace)" />
                <xsl:value-of select="$by" />
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="substring-after($text,$replace)" />
                    <xsl:with-param name="replace" select="$replace" />
                    <xsl:with-param name="by" select="$by" />
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$text" />
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    

    
</xsl:stylesheet>