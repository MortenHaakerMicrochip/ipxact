<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs" version="2.0">


    <xsl:template match="@*|node()">
        <xsl:apply-templates select="@*|node()" mode="dita-topic"/>
    </xsl:template>

    <!-- identity transform -->
    <xsl:template match="@*|node()" mode="dita-topic">
        <xsl:copy copy-namespaces="no">
            <xsl:apply-templates select="@*|node()" mode="dita-topic"/>
        </xsl:copy>
    </xsl:template>

    <!-- When finding an image w/ title before it, wrap them both in a figure element -->
    <xsl:template match="//image[preceding-sibling::*[1][name() = 'title']][not(parent::fig) and not(parent::example)]"
        mode="dita-topic">
        <xsl:variable name="imgTitle" select="./preceding-sibling::title[1]"/>
        <fig id="{generate-id()}">
            
            <!-- Make wide images be landscape for PDF output -->
            <xsl:if test="number(replace(./@width,'px','')) &gt;= 625">
                <xsl:attribute name="outputclass">landscape</xsl:attribute>
            </xsl:if>
            
            <xsl:copy-of select="$imgTitle" copy-namespaces="no"/>
            <!-- <xsl:copy-of select="."/> -->
            <xsl:copy copy-namespaces="no">
                <xsl:apply-templates select="@*|node()" mode="dita-topic"/>
            </xsl:copy>
        </fig>
    </xsl:template>


    <!-- When finding a table w/ title before it, move the title inside table element -->
    <xsl:template name="insert-table-title">
        <xsl:if test=".[preceding-sibling::*[1][name() = 'title']]">
            <xsl:variable name="tableTitle" select="./preceding-sibling::title[1]"/>
            <xsl:copy-of select="$tableTitle" copy-namespaces="no" />
        </xsl:if>
    </xsl:template>
    
    <!-- Don't process titles before images or tables -->
    <xsl:template match="//title[following-sibling::*[1][name() = 'image']][not(parent::fig) and not(parent::example)]"
        mode="dita-topic"/>
    <xsl:template match="//title[following-sibling::*[1][name() = 'table']]" mode="dita-topic"/>
    
    
    
    <!-- When finding an image w/ example before it, move img inside example and fix title element -->
    <xsl:template match="//image[preceding-sibling::*[1][name() = 'example']][not(parent::example)]"
        mode="dita-topic">
        <xsl:variable name="exampleTitle" select="./preceding-sibling::example[1]"/>
        <example id="{generate-id()}">
            
            <!-- Make wide images be landscape for PDF output -->
            <xsl:if test="number(replace(./@width,'px','')) &gt; 626">
                <xsl:attribute name="outputclass">landscape</xsl:attribute>
            </xsl:if>
            
            <!-- Convert the <example> element before img  -->
            <title><xsl:value-of select="$exampleTitle"/></title>
            <xsl:copy copy-namespaces="no">
                <xsl:apply-templates select="@*|node()" mode="dita-topic"/>
            </xsl:copy>
        </example>
    </xsl:template>
    
    <!-- Don't process examples w/o titles, to get rid of orphan examples after correcting -->
    <xsl:template match="//example[not(title)]" mode="dita-topic"/>
    
    
    

    <!-- Don't process badly placed <p> elements, but put them in a variable if not empty and move text inside conbody -->
    <xsl:variable name="invalid-para" select="//concept/p[text()]/text()" />
    <xsl:template match="//concept/p" mode="dita-topic" />
    <xsl:template match="//concept/conbody[preceding-sibling::p]" mode="dita-topic">
        <xsl:copy copy-namespaces="no">
            <p><xsl:value-of select="$invalid-para"/></p>
            <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
    </xsl:template>
    

    <!-- Get rid of intex terms and their containing ph elements -->
    <xsl:template match="//indexterm" mode="dita-topic"/>
    <xsl:template match="//ph[@outputclass='phindex']" mode="dita-topic"/>
    
    
    <!-- Make wide images be landscape for PDF output -->
    <xsl:template match="//fig[image[number(replace(@width,'px','')) &gt;= 625]]" mode="dita-topic">
        <xsl:copy copy-namespaces="no">
            <xsl:attribute name="outputclass">landscape</xsl:attribute>
            <xsl:apply-templates select="@*|node()" mode="dita-topic"/>
        </xsl:copy>
    </xsl:template>
    
    
    
    <!-- Remove column width specifications from tables, formatting works better w/o it. -->
    <xsl:template match="//@colwidth" mode="dita-topic">
        <xsl:variable name="colwidth-raw" ><xsl:value-of select="replace(.,'pt','')"/></xsl:variable>
        <xsl:attribute name="colwidth"></xsl:attribute>
    </xsl:template>
    
    <!-- Make wide tables be landscape for PDF output -->
    <xsl:template match="//table[count(descendant::row)&gt;1]" mode="dita-topic" priority="2">
        
        <xsl:variable name="colwidths">
            <xsl:for-each select=".//@colwidth">
                <colwidth><xsl:value-of select="number(replace(.,'pt',''))"/></colwidth>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="colwidth-sum">
            <xsl:value-of select="sum($colwidths/colwidth)"/>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="$colwidth-sum&gt;=600">
                <xsl:copy copy-namespaces="no">
                    <xsl:attribute name="outputclass">landscapetable</xsl:attribute>
                    <xsl:apply-templates select="@*" mode="dita-topic"/>
                    <xsl:call-template name="insert-table-title"/>
                    <xsl:apply-templates select="node()" mode="dita-topic"/>
                </xsl:copy>
            </xsl:when>
            <xsl:when test="$colwidth-sum&gt;=450 and $colwidth-sum&lt;600">
                <xsl:copy copy-namespaces="no">
                    <xsl:attribute name="outputclass">widetable</xsl:attribute>
                    <xsl:apply-templates select="@*" mode="dita-topic"/>
                    <xsl:call-template name="insert-table-title"/>
                    <xsl:apply-templates select="node()" mode="dita-topic"/>
                </xsl:copy>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy copy-namespaces="no">
                    <xsl:apply-templates select="@*" mode="dita-topic"/>
                    <xsl:call-template name="insert-table-title"/>
                    <xsl:apply-templates select="node()" mode="dita-topic"/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:template>
    

    <!-- Unencapsulate <note> elements from the wrapping FM tables -->
    <xsl:template match="//table[tgroup/tbody/row/entry/note][count(descendant::row)&lt;=1]" mode="dita-topic">
        <xsl:copy-of select=".//note" copy-namespaces="no" />
    </xsl:template>
    

    <!-- Remove erroneous top level bullets that show up in front of bullet2 sub-bullets -->
    <xsl:template match="//ul/li[following-sibling::li[1][ul/li][not(matches(text()[1], '[\S]+'))]]" mode="dita-topic">
        <!-- grabs a copy of all current children of the previous bullet -->
        <!-- Then copies in the sub-bullets of the phantom-bullet -->
        <li><xsl:copy-of select="./@* | ./node()" copy-namespaces="no"/><ul>
            <xsl:copy-of select="./following-sibling::li[1][not(matches(text()[1], '[\S]+'))]/ul/*" copy-namespaces="no"/></ul></li>
    </xsl:template>

    <!-- Don't process the sub-bullets we copied elsewhere -->
    <xsl:template match="//ul/li[ul/li][not(matches(text()[1], '[\S]+'))]" mode="dita-topic"/>


    <!-- Remove erroneous top level Numbers that show up in front of bullet2 sub-bullets -->
    <xsl:template match="//ol/li[following-sibling::li[1][ul/li][not(matches(text()[1], '[\S]+'))]]" mode="dita-topic">
        <!-- grabs a copy of all current children of the previous bullet -->
        <!-- Then copies in the sub-bullets of the phantom-bullet -->
        <li><xsl:copy-of select="./@* | ./node()" copy-namespaces="no" /><ul>
            <xsl:copy-of select="./following-sibling::li[1][not(matches(text()[1], '[\S]+'))]/ul/*" copy-namespaces="no"/></ul></li>
    </xsl:template>
    
    <!-- Don't process the sub-bullets we copied elsewhere -->
    <xsl:template match="//ol/li[ul/li][not(matches(text()[1], '[\S]+'))]" mode="dita-topic"/>
    
    
    <!-- Sicover fixes -->
    <xsl:template match="/concept[@id='sicover' and not(title)]" mode="dita-topic">
        <concept id='sicover' outputclass='introduction'>
            <title>Revision History</title>
            <!-- <conbody> -->
            <xsl:apply-templates select="node()" mode="dita-topic"/>         
                
        </concept>
    </xsl:template>
    
    <!-- Get rid of abstracts, they are never right so just unwrap them -->
    <xsl:template match="//abstract" mode="dita-topic">
        <xsl:apply-templates select="./node()" mode="dita-topic"/>
    </xsl:template>


    <!-- Duplicate ID fixing -->
    <xsl:key name="idKey" match="//table" use="@id"></xsl:key>
    <xsl:template match="//table/@id[key('idKey', .)[2]]" mode="dita-topic">
        <xsl:variable name="myId" select="generate-id(..)" />
        <xsl:attribute name="id">
            <xsl:value-of select="." />
            <xsl:text>-</xsl:text>
            <xsl:for-each select="key('idKey', .)">
                <xsl:if test="generate-id() = $myId">
                    <xsl:value-of select="position()" />
                </xsl:if>
            </xsl:for-each>
        </xsl:attribute>
    </xsl:template>
    
    <!-- Remove comment text -->
    <xsl:template match="//comment()" mode="dita-topic"/>
    
    <!-- Remove body text errors resulting from register table conversion -->
    <xsl:template match="//table/p" mode="dita-topic" />
    


    <!-- Rename image references to SVG from WMF and call SVG fixing script on the image -->
    <xsl:template match="//image/@href" mode="dita-topic">
        <xsl:variable name="href">
            <xsl:value-of select="."/>
        </xsl:variable>
        <xsl:variable name="svg-href" select="replace(replace($href, '.eps', '.svg'), '.wmf', '.svg')"/>
        <xsl:attribute name="href">
            <xsl:value-of select="$svg-href"/>
        </xsl:attribute>
    </xsl:template>
    
</xsl:stylesheet>
