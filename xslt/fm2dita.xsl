<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
       
    <xsl:import href="fm2dita-map.xsl" />
    <xsl:import href="fm2dita-topic.xsl" />
    <xsl:output method="xml" indent="no"/>
    <xsl:strip-space elements="*"/>
    
    <!-- This code isn't working when executed via refactoring, even with the mode present. not sure why. -->
    <!-- 
    <xsl:template match="/map/title[not(text()='Mif2Go Test Book')]" mode="create-placeholders">
        
        <xsl:variable name="macroname" select="../@id" />
        
        <xsl:result-document href="./{$macroname}_hwinterface.dita">
            <concept><title>Hardware Interface</title><conbody></conbody></concept>
        </xsl:result-document>
        <xsl:result-document href="./{$macroname}_hw_ports.dita">
            <concept><title>Ports</title><conbody></conbody></concept>
        </xsl:result-document>
        <xsl:result-document href="./{$macroname}_hw_parameters.dita">
            <concept><title>Parameters</title><conbody></conbody></concept>
        </xsl:result-document>      
        
        <xsl:apply-templates />
        
    </xsl:template>
    --> 
    <xsl:template match="/" exclude-result-prefixes="#all">
        <xsl:choose>
            <xsl:when test="/map">
                <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE map PUBLIC &quot;-//OASIS//DTD DITA Map//EN&quot; &quot;map.dtd&quot;&gt;</xsl:text>
                <xsl:apply-templates mode="dita-map" select="."/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text disable-output-escaping="yes">&lt;!DOCTYPE concept PUBLIC &quot;-//Atmel//DTD DITA Mathml Concept//EN&quot; &quot;AtmelConcept.dtd&quot;&gt;</xsl:text>
                <xsl:apply-templates mode="dita-topic" select="."/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
